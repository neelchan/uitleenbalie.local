Uitleenbalie.
==========

I. About
--------

###Neeltje Chanterie

- *Opleidingsonderdeel:* ProLab
- *Academiejaar* 2015-2016
- *Opleiding:* Bachelor in de grafische en digitale media
- *Afstudeerrichting:* Multimediaproductie
- *Keuzeoptie:* proDEV
- *Opleidingsinstelling:* Arteveldehogeschool

II. Installation
----------------

### Project installation

```
$ c
$ git clone https://neelchan@bitbucket.org/neelchan/uitleenbalie.local.git
$ uitleenbalie.local
$ artestead make --type laravel --ip 192.162.10.20
$ composer update
```

### Server starten + DB vullen

```
$ uitleenbalie.local
$ vagrant up
$ composer_update
$ cd uitleenbalie.local/www
$uitleenbalie.local/www php artisan migrate:refresh --seed
```


URI's
-----

 - FRONT: <http://uitleenbalie.local>
 - BACK <https://uitleenbalie.local/backoffice/home>

[Artevelde Laravel Homestead]: http://www.gdm.gent/artestead/