@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height">
    <div class="row content-detail">
        <div class=" col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit {!! $product->name !!}</div>
                <div class="panel-body">

                    <div class="col-md-10 col-md-offset-1 mb">

                        {!! Form::model($product, ['method' => 'PUT', 'action' => ['ProductController@updateProduct', $product->id], 'files' => 'true']) !!}
                        <div class="form-group">
                            {!! Form::label('image', 'Highlighted image', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::file('image', ['class' => 'form-control']) !!}
                            </div>
                        </div>


                        {!! Form::submit('Submit', ['class' => 'btn btn-primary pull-right mt']) !!}
                        <a href="{{ URL::previous() }}" class="btn btn-theme04 mt">Back</a>

                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection