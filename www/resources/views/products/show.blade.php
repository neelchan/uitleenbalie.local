@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div class="row content-panel">
                <div class="col-xs-12 profile-text">
                    <a href="{{ URL::previous() }}" class="btn btn-theme03"><i class="fa fa-arrow-left"></i> Back</a>
                    <hr>
                    <h2>
                        {{$product->name}}
                        <span class="pull-right">
                            <a href="/backoffice/products/{!! $product->id !!}/edit" class="btn btn-theme03">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="/backoffice/products/{!! $product->id !!}/editImage"
                               class="btn btn-theme02"><i class="fa fa-camera-retro"></i>
                            </a>
                            <div aria-expanded="false" class="pull-right ml">
                                {!! Form::model($product, ['method' => 'DELETE', 'action' => ['ProductController@softDelete', $product->id], 'onsubmit' => 'return AlertDelete()']) !!}
                                {!! Form::button('<i class="fa fa-ban"></i>', array(
                                'type' => 'submit',
                                'aria-expanded' => 'false',
                                'class'=> 'btn btn-theme04',
                                'onclick'=>'return confirm("Are you sure you want to delete this?")'
                                )) !!}
                                {!! Form::close() !!}
                            </div>
                        </span>
                    </h2>
                    {{--<h4>{{$user->email}}</h4>--}}
                    <p>{{$product->description}}</p>
                    {{--<hr style="border-color: #d3d3d3">--}}
                </div>
                <! --/col-xs-12 -->

                <div class="col-md-8 profile-text mb mt">
                    <table class="table mb mt">
                        <tr>
                            <th class="col-xs-5">ID</th>
                            <td class="col-xs-7">{{$product->id}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">VM/ICT-nummer</th>
                            <td class="col-xs-7">{{$product->product_code}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Naam</th>
                            <td class="col-xs-7">{{$product->name}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Merk</th>
                            <td class="col-xs-7">{{$product->brand}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Aankoopprijs</th>
                            <td class="col-xs-7">&euro; {{$product->purchase_price}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Aankoopdatum</th>
                            <td class="col-xs-7">{{$product->purchase_date->format('d/m/Y')}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Kan uitgeleend worden door:</th>
                            <td class="col-xs-7">
                                @if ($product->CanBeLentByTeacher == 1 && $product->CanBeLentByStudent == null)
                                    Lector
                                @endif
                                @if ($product->CanBeLentByStudent == 1 && $product->CanBeLentByTeacher == null)
                                    Student
                                @endif
                                @if ($product->CanBeLentByTeacher && $product->CanBeLentByStudent == 1)
                                    Lector & Student
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Tags</th>
                            <td class="col-xs-7">{{$product->tags}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Type</th>
                            <td class="col-xs-7">{{$product->type}}</td>
                        </tr>
                    </table>
                    <table class="table mb mt">
                        <thead>
                        <tr>
                            <th>Modelnummer</th>
                            <th>QR-code</th>
                            <th>Serienummer</th>
                            <th>Productiedatum</th>
                            <th>Aangemaakt</th>
                            <th>Aangepast</th>
                            @if($product->deleted_at != null)
                                <th>Verwijderd</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>{{$product->brand_number}}</td>
                            <td>{{$product->QR_code}}</td>
                            <td>{{$product->serial_number}}</td>
                            <td>{{$product->production_date->format('d/m/Y')}}</td>
                            <td>{{ $product->created_at->format('d/m/Y H:i')}}</td>
                            <td>{{$product->updated_at->format('d/m/Y H:i')}}</td>
                            @if($product->deleted_at != null)
                                <td>{{$product->deleted_at->format('d/m/Y H:i')}}</td>
                            @endif

                        </tr>
                        </tbody>
                    </table>
                </div>
                <! --/col-md-8 -->

                <div class="col-md-4 profile-text centered mb mt">
                    <div class="left-divider mt">
                        <img src="/uploads/products/{{$product->image}}"
                             class="img-responsive">
                    </div>
                </div>
                <! --/col-md-4 -->


            </div><!-- /row -->
        </div>
    </div>
</section>
@endsection
