@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div class="row content-panel">
                <div id="all" class="tab-pane active">
                    <div class="col-md-12 detailed">
                        <div class="table-responsive">
                            <h2>Inactieve producten
                                            <span class="pull-right">
                                                <a href="/products/create" class="btn btn-theme03">Nieuw product</a>
                                            </span>
                            </h2>
                            <div class="search">

                                {{--{{ Form::model(null, array('route' => array('products.search'))) }}--}}
                                {{--{{ Form::text('query', null, array( 'placeholder' => 'Search query...' )) }}--}}
                                {{--{{ Form::submit('Search') }}--}}
                                {{--{{ Form::close() }}--}}


                            </div>

                            {{--<table class="table">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                            {{--<th>@sortablelink ('id')</th>--}}
                            {{--<th>@sortablelink ('name')</th>--}}
                            {{--<th>E-mail</th>--}}
                            {{--<th>Created</th>--}}
                            {{--<th>Updated</th>--}}
                            {{--<th>Actions</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}

                            {{--@foreach ($products as $product)--}}
                            {{--<tr>--}}
                            {{--<td>{{$product->id}}</td>--}}
                            {{--<td>{{$product->name}}</td>--}}
                            {{--<td>{{$product->email}}</td>--}}
                            {{--<td>{{$product->created_at}}</td>--}}
                            {{--<td>{{$product->updated_at}}</td>--}}
                            {{--<td><a href="products/{{$product->id}}"><i class="fa fa-eye"></i></a></td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                            {{--</tbody>--}}
                            {{--</table>--}}
                            {{--<div class="col-md-12 text-center">--}}
                            {{--{!! $products->appends(\Input::except('page'))->render() !!}--}}
                            {{--</div>--}}

                            @if ($products->count())
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>@sortablelink ('id', '#')</th>
                                        <th>@sortablelink ('name', 'Naam')</th>
                                        <th>@sortablelink ('product_code', 'Code')</th>
                                        <th>@sortablelink ('barcode')</th>
                                        <th>@sortablelink ('purchase_price', 'Aankoopprijs')</th>
                                        <th>@sortablelink ('type')</th>
                                        <th>@sortablelink ('active', 'Status')</th>
                                        <th>Aangemaakt</th>
                                        <th>Aangepast</th>
                                        <th>Acties</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach ($products as $product)
                                        <tr>
                                            <td>{{$product->id}}</td>
                                            <td>{{$product->name}}</td>
                                            <td>{{$product->product_code}}</td>
                                            <td>{{$product->barcode}}</td>
                                            <td>&euro; {{$product->purchase_price}}</td>
                                            <td>{{$product->type}}</td>
                                            <td>
                                                @if($product->active == 1)
                                                    <span class="label label-success">Active</span>
                                                @else
                                                    <span class="label label-default">Inactive</span>
                                                @endif
                                            </td>
                                            <td>{{$product->created_at->format('m/d/Y')}}</td>
                                            <td>{{$product->updated_at->format('m/d/Y')}}</td>
                                            <td style="width: 11.25%">
                                                <a href="../{{$product->id}}" class="btn btn-default"
                                                   aria-expanded="false">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a class="btn btn-default"
                                                   href="/backoffice/products/{!! $product->id !!}/edit"
                                                   aria-expanded="false">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <div aria-expanded="false" class="pull-right">
                                                    {!! Form::model($product, ['method' => 'PATCH', 'action' => ['ProductController@restore', $product->id], 'onsubmit' => 'return AlertDelete()']) !!}
                                                    {!! Form::button('<i class="fa fa-ban"></i>', array(
                                                    'type' => 'submit',
                                                    'aria-expanded' => 'false',
                                                    'class'=> 'btn btn-default',
                                                    'onclick'=>'return confirm("Are you sure you want to restore this?")'
                                                    )) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="col-md-12 text-center">
                                    {!! $products->links() !!}
                                </div>
                            @else
                                There are no inactive users
                            @endif
                        </div>
                        {{--table-responsive--}}
                    </div>
                    {{--col-md-12 detailed--}}
                </div>
                {{--tab-panel--}}
            </div>
            {{--/row content-panel--}}
        </div>
        {{--/col-lg-12--}}
    </div>
    {{--/row--}}
</section>
@endsection
