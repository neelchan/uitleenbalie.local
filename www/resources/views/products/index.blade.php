@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div class="row content-panel">
                <div id="all" class="tab-panel">
                    <div class="col-md-12 detailed">
                        <div class="table-responsive">
                            <h2>Alle producten
                                <span class="pull-right">
                                    <a href="/backoffice/products/create" class="btn btn-theme03">Nieuw product</a>
                                </span>
                            </h2>

                            {{--<div class="search">--}}
                            {{--{{ Form::model(null, array('route' => array('users.search'))) }}--}}
                            {{--{{ Form::text('query', null, array( 'placeholder' => 'Search query...' )) }}--}}
                            {{--{{ Form::submit('Search') }}--}}
                            {{--{{ Form::close() }}--}}
                            {{--</div>--}}
                            <div class="row no-gutter">



                                {!! Form::open(['action' => 'ProductController@search', 'method' => 'GET', 'class' => 'form-group col-md-4 row no-gutter', 'role' => 'search']) !!}
                                <div class="col-md-10">
                                    {!! Form::text('term', Request::get('term'), ['class' => 'form-control', 'placeholder' => 'Zoek ...']) !!}
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-theme03"><i class="fa fa-search"></i></button>
                                </div>

                                {!! Form::close() !!}


                                <div class="col-md-1 centered">
                                    <a href="{{URL::route('products.export')}}" class="btn btn-theme02"><i
                                                class="fa fa-download"></i></a>
                                    {{--<a href="{{URL::route('users.export')}}" class="btn btn-theme"><i class="fa fa-upload"></i></a>--}}
                                </div>
                                {{--<div class="form-group col-md-4 row no-gutter">
                                    {!! Form::model(['method' => 'POST', 'action' => 'ProductsController@postUploadCsv']) !!}
                                    {!! Form::file('file', ['class' => 'col-xs-10 pull-right']) !!}
                                    {!! Form::button('<i class="fa fa-upload"></i>', ['class' => 'btn btn-theme03 pull-left']) !!}
                                    {!! Form::close() !!}
                                </div>
--}}

                                <div class="form-group col-md-3 row no-gutter">
                                    <form action="{{ URL::to('backoffice/importExcelProducts') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input class="col-xs-10 pull-left" type="file" name="import_file" />
                                        <button class="btn btn-theme04 pull-right"><i class="fa fa-upload"></i></button>
                                    </form>
                                </div>
                            </div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>@sortablelink ('id', '#')</th>
                                    <th>@sortablelink ('product_code', 'AVM/ICT-nummer')</th>
                                    <th>@sortablelink ('name', 'Naam')</th>
                                    <th>@sortablelink ('brand', 'Merk')</th>
                                    {{--<th>Aankoopprijs</th>--}}
                                    <th>@sortablelink ('CanBeLentByTeacher', 'Lector')</th>
                                    <th>@sortablelink ('CanBeLentByStudent', 'Student')</th>
                                    <th>@sortablelink ('type')</th>
                                    <th>@sortablelink ('active', 'Status')</th>
                                    <th>Aangemaakt</th>
                                    <th>Aangepast</th>
                                    <th>Acties</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($products as $product)
                                    <tr>
                                        <td>{{$product->id}}</td>
                                        <td>{{$product->product_code}}</td>
                                        <td>{{$product->name}}</td>
                                        <td>{{$product->brand}}</td>
                                        {{--<td>&euro; {{$product->purchase_price}}</td>--}}
                                        <td>@if ($product->CanBeLentByTeacher == 1)
                                                ja @else Nee
                                            @endif</td>
                                        <td>@if ($product->CanBeLentByStudent == 1)
                                                ja @else Nee
                                            @endif</td>
                                        <td>{{$product->type}}</td>
                                        <td>
                                            @if($product->active == 1)
                                                <span class="label label-success">Active</span>
                                            @else
                                                <span class="label label-default">Inactive</span>
                                            @endif
                                        </td>
                                        <td>{{$product->created_at->format('m/d/Y')}}</td>
                                        <td>{{$product->updated_at->format('m/d/Y')}}</td>
                                        <td style="width: 11.45%">
                                            <a href="products/{{$product->id}}" class="btn btn-default"
                                               aria-expanded="false">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                            <a class="btn btn-default"
                                               href="/backoffice/products/{!! $product->id !!}/edit"
                                               aria-expanded="false">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            <div aria-expanded="false" class="pull-right">
                                                {!! Form::model($product, ['method' => 'DELETE', 'action' => ['ProductController@softDelete', $product->id], 'onsubmit' => 'return AlertDelete()']) !!}
                                                {!! Form::button('<i class="fa fa-ban"></i>', array(
                                                'type' => 'submit',
                                                'aria-expanded' => 'false',
                                                'class'=> 'btn btn-default',
                                                'onclick'=>'return confirm("Are you sure you want to delete this?")'
                                                )) !!}
                                                {!! Form::close() !!}
                                            </div>
                                            {{--<div aria-expanded="false" class="pull-right">--}}
                                            {{--{!! Form::model($product, ['method' => 'DELETE', 'action' => ['ProductController@destroyProduct', $product->id], 'onsubmit' => 'return AlertDelete()']) !!}--}}
                                            {{--{!! Form::button('<i class="fa fa-times"></i>', array(--}}
                                            {{--'type' => 'submit',--}}
                                            {{--'aria-expanded' => 'false',--}}
                                            {{--'class'=> 'btn btn-default',--}}
                                            {{--'onclick'=>'return confirm("Are you sure you want to delete this?")'--}}
                                            {{--)) !!}--}}
                                            {{--{!! Form::close() !!}--}}
                                            {{--</div>--}}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>
                            <div class="col-md-12 text-center">
                                {!! $products->appends(\Input::except('page'))->render() !!}

                            </div>
                            {{--/pagination--}}
                        </div>
                        {{--table-responsive--}}
                    </div>
                    {{--col-md-12 detailed--}}
                </div>
                {{--tab-panel--}}
            </div>
            {{--/row content-panel--}}
        </div>
        {{--/col-lg-12--}}
    </div>
    {{--/row--}}
</section>
@endsection
