@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height">
    <div class="row content-detail">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Nieuw product</div>
                <div class="panel-body">

                    <div class="col-md-10 col-md-offset-1">

                        {!! Form::open(['url' => 'backoffice/products', 'files' => 'true', 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            {!! Form::label('product_code', 'Product code:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('product_code', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Naam:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('brand', 'Merk:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('brand', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('brand_number', 'Merknummer:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('brand_number', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('serial_number', 'Serienummer:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('serial_number', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('production_date', 'Productiedatum:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::date('production_date', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('description', 'Beschrijving:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('description', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('tags', 'Tags:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('tags', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('QR_code', 'QR-code:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('QR_code', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('purchase_price', 'Aankoopprijs:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('purchase_price', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('purchase_date', 'Aankoopdatum:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::date('purchase_date', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-4">
                                {!! Form::checkbox('CanBeLentByTeacher', 1, null, ['class' => 'col-md-1 field']) !!}
                                {!! Form::label('CanBeLentByTeacher', 'Kan uitgeleend worden door lector') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-4">
                                {!! Form::checkbox('CanBeLentByStudent', 1, null, ['class' => 'col-md-1 field'])  !!}
                                {!! Form::label('CanBeLentByStudent', 'Kan uitgeleend worden door student') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('type', 'Type product', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::select('type',['AVM'=>'AVM','ICT'=>'ICT'], null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('image', 'Afbeelding', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::file('image', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-4">
                                {!! Form::checkbox('active', 1, 'checked', ['class' => 'col-md-1 field'])  !!}
                                {!! Form::label('active', 'Actief plaatsen?') !!}
                            </div>
                        </div>

                        {!! Form::submit('Submit', ['class' => 'btn btn-theme03 pull-right']) !!}
                        <a href="{{ URL::previous() }}" class="btn btn-theme04">Back</a>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection