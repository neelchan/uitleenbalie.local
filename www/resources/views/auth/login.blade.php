@extends('layouts.auth')

@section('content')

    <form class="form-login" role="form" method="POST" action="{{ url('/login') }}">
        {!! csrf_field() !!}
        <h2 class="form-login-heading">Login Uitleenbalie</h2>
        <div class="login-wrap">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label class="control-label">E-Mail Address</label>
                <input type="email" class="form-control" name="email" value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label class="control-label">Password</label>
                <input type="password" class="form-control" name="password">

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group">
                <div class="checkbox">
                    <label class="pull-left">
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                    <a class="pull-right" href="{{ url('/password/reset') }}">Forgot Your Password?</a>

                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-theme pull-right">
                    <i class="fa fa-btn fa-sign-in"></i> Login
                </button>
                <br>
            </div>
        </div>
    </form>
@endsection
