<!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Uitleenbalie</title>

    <link rel="stylesheet" href="{{ URL::asset('frontoffice/css/bootstrap.min.css')}}" type="text/css">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
          rel='stylesheet' type='text/css'>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="{{ URL::asset('frontoffice/css/animate.min.css')}}" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('frontoffice/css/main.css')}}" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    {{--<link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>--}}
    {{--<link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'--}}
          {{--rel='stylesheet' type='text/css'>--}}

</head>
<body id="page-top">

<nav id="mainNav" class="navbar navbar-default navbar-fixed-top affix">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="{{ url('/home') }}">Uitleenbalie</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li {{ (Request::is('*home*') ? 'class=active' : '') }}>
                    <a href="{{ url('/home') }}">Home</a>
                </li>
                <li {{ (Request::is('*catalogus*') ? 'class=active' : '') }}>
                    <a href="{{ url('/catalogus') }}">Catalogus</a>
                </li>
                @if (Auth::guest())
                    <li {{ (Request::is('*login*') ? 'class=active' : '') }}>
                        <a href="{{ url('/login') }}">Login</a>
                    </li>
                @else
                    <li {{ (Request::is('*account*') ? 'class=active' : '') }}>
                        <a href="{{ url('/account/'. Auth::user()->id) }}">Account</a>
                    </li>

                    <li {{ (Request::is('*logout*') ? 'class=active' : '') }}>
                        <a href="{{ url('/logout') }}">Logout</a>
                    </li>
                @endif

                <li {{ (Request::is('*contact*') ? 'class=active' : '') }}>
                    <a href="{{ url('/contact') }}">Contact</a>
                </li>
                <!--<div class="input-group">-->
                <!--<input type="text" class="form-control">-->
                <!--<span class="input-group-btn">-->
                <!--<button class="btn btn-default" type="button">-->
                <!--<span class="glyphicon glyphicon-search"></span>-->
                <!--</button>-->
                <!--</span>-->
                <!--</div>-->
                {!! Form::open(['route' => 'catalogus.search', 'method' => 'GET', 'class' => 'navbar-form navbar-right', 'role' => 'search']) !!}
                    <div class="input-group">
                        {!! Form::text('term', Request::get('term'), ['class' => 'form-control', 'placeholder' => 'Search ...']) !!}
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                {!! Form::close() !!}
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
            @yield('content')
    <!-- /MAIN CONTENT -->

<footer class="footer">
    <div class="container">
        <p class="text-muted"><i class="fa fa-copyright"></i> Gemaakt door Neeltje Chanterie, in opdracht van
            de
            Arteveldehogeschool
            <span class="pull-right"><a href="#page-top "><i class="fa phpdebugbar-fa-arrow-up"></i></a></span>
        </p>

    </div>
</footer>

<!-- jQuery -->
<script src="{{ URL::asset('frontoffice/js/jquery.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('frontoffice/js/bootstrap.min.js')}}"></script>

<!-- Plugin JavaScript -->
<script src="{{ URL::asset('frontoffice/js/jquery.easing.min.js')}}"></script>
<script src="{{ URL::asset('frontoffice/js/jquery.fittext.js')}}"></script>
<script src="{{ URL::asset('frontoffice/js/wow.min.js')}}"></script>

<!-- Custom Theme JavaScript -->
<script src="{{ URL::asset('frontoffice/js/creative.js')}}"></script>

</body>

</html>
