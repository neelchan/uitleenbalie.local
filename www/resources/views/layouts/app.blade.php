<!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Uitleenbalie - Backoffice</title>


    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('backoffice/css/bootstrap.css') }}" rel="stylesheet">
    <!--external css-->
    <link rhref="{{ URL::asset('backoffice/css/zabuto_calendar.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('backoffice/css/main.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('backoffice/css/style-responsive.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
            rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>

</head>
<body id="app-layout">

<section id="container">
    <!-- **********************************************************************************************************************************************************
    TOP BAR CONTENT & NOTIFICATIONS
    *********************************************************************************************************************************************************** -->
    <!--header start-->
    <header class="header black-bg">
        <div class="sidebar-toggle-box">
            <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
        </div>
        <!--logo start-->
        <a href="{{ url('/backoffice/home') }}" class="logo"><b>Uitleenbalie</b></a>
        <!--logo end-->
        <div class="nav notify-row pull-right" id="top_menu">
            <!--  notification start -->
            <ul class="nav top-menu">
                <!-- settings start -->
                @if (Auth::guest())
                    <li>Ga naar de login pagina om je dashboard te kunnen zien.</li>
                @else
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="fa fa-tasks"></i>
                            <span class="badge bg-theme">4</span>
                        </a>
                        <ul class="dropdown-menu extended tasks-bar">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
                                <p class="green">You have 4 pending tasks</p>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div class="desc">Uitleenbalie Admin Panel</div>
                                        <div class="percent">40%</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                             aria-valuenow="40"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                            <span class="sr-only">40% Complete (success)</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div class="desc">Database Update</div>
                                        <div class="percent">60%</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                             aria-valuenow="60"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div class="desc">Product Development</div>
                                        <div class="percent">80%</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-info" role="progressbar"
                                             aria-valuenow="80"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div class="desc">Payments Sent</div>
                                        <div class="percent">70%</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="70"
                                             aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                            <span class="sr-only">70% Complete (Important)</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="external">
                                <a href="#">See All Tasks</a>
                            </li>
                        </ul>
                    </li>
                    <!-- settings end -->
                    <!-- inbox dropdown start-->
                    <li id="header_inbox_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <i class="fa fa-envelope-o"></i>
                            <span class="badge bg-theme">5</span>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <div class="notify-arrow notify-arrow-green"></div>
                            <li>
                                <p class="green">You have 5 new messages</p>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="photo"><img alt="avatar"
                                                             src="https://d13yacurqjgara.cloudfront.net/users/124355/screenshots/2199042/profile.png"></span>
                                    <span class="subject">
                                    <span class="from">Zac Snider</span>
                                    <span class="time">Just now</span>
                                    </span>
                                    <span class="message">
                                        Hi mate, how is everything?
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="photo"><img alt="avatar"
                                                             src="https://d13yacurqjgara.cloudfront.net/users/124355/screenshots/2199042/profile.png"></span>
                                    <span class="subject">
                                    <span class="from">Divya Manian</span>
                                    <span class="time">40 mins.</span>
                                    </span>
                                    <span class="message">
                                     Hi, I need your help with this.
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="photo"><img alt="avatar"
                                                             src="https://d13yacurqjgara.cloudfront.net/users/124355/screenshots/2199042/profile.png"></span>
                                    <span class="subject">
                                    <span class="from">Dan Rogers</span>
                                    <span class="time">2 hrs.</span>
                                    </span>
                                    <span class="message">
                                        Love your new Dashboard.
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="photo"><img alt="avatar"
                                                             src="https://d13yacurqjgara.cloudfront.net/users/124355/screenshots/2199042/profile.png"></span>
                                    <span class="subject">
                                    <span class="from">Dj Sherman</span>
                                    <span class="time">4 hrs.</span>
                                    </span>
                                    <span class="message">
                                        Please, answer asap.
                                    </span>
                                </a>
                            </li>
                            <li>
                                <a href="#">See all messages</a>
                            </li>
                        </ul>
                    </li>
                    <!-- inbox dropdown end -->
                    <li>
                        <a href="{{ url('/backoffice/logout') }}">
                            <i class="fa fa-sign-out"></i>
                        </a>
                    </li>
                @endif
            </ul>
            <!--  notification end -->
        </div>
    </header>
    <!--header end-->

    <!-- **********************************************************************************************************************************************************
    MAIN SIDEBAR MENU LEFT
    *********************************************************************************************************************************************************** -->
    <!--sidebar start-->
    <aside>
        <div id="sidebar" class="nav-collapse ">
            <!-- sidebar menu start-->
            <ul class="sidebar-menu" id="nav-accordion">
                @if (Auth::guest())
                    <li class="mt">
                        <a href="{{ url('/login') }}">
                            <i class="fa fa-sign-in"></i>
                            <span>Login</span>
                        </a>
                    </li>
                    <li class="mt">
                        <a href="{{ url('/register') }}">
                            <i class="fa fa-sign-in"></i>
                            <span>Register</span>
                        </a>
                    </li>
                @else
                    <p class="centered"><a href="{{ url('/backoffice/users/'. Auth::user()->id) }}"><img
                                    src="https://d13yacurqjgara.cloudfront.net/users/124355/screenshots/2199042/profile.png"
                                    class="img-circle" width="125"></a></p>
                    <h5 class="centered">{{ Auth::user()->name }} </h5>

                    <li class="mt">
                        <a href="{{ url('/backoffice/home') }}" {{ (Request::is('*home*') ? 'class=active' : '') }}>
                            <i class="fa fa-dashboard"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="sub-menu">
                        <a href="#" {{ (Request::is('*users*') ? 'class=active' : '') }}>
                            <i class="fa fa-users"></i>
                            <span>Gebruikers</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{ url('/backoffice/users') }}">Alle
                                    gebruikers</a>
                            </li>
                            <li>
                                <a href="{{ url('/backoffice/users/deleted') }}">Verwijderd</a>
                            </li>
                            <li>
                                <a href="{{ url('/backoffice/users/inactive') }}">Inactief</a>
                            </li>
                            <li>
                                <a href="{{ url('/backoffice/roles') }}">Rollen</a>
                            </li>
                            <li>
                                <a href="{{ url('/backoffice/users/admins') }}">Admin</a>
                                <ul class="sub">
                                    <li><a href="{{ url('/backoffice/users/admins') }}">Alle
                                            gebruikers</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/backoffice/users/admins/deleted') }}">Verwijderd</a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/backoffice/users/admins/inactive') }}">Inactief</a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" {{ (Request::is('*products*') ? 'class=active' : '') }}>
                            <i class="fa fa-database"></i>
                            <span>Producten</span>
                        </a>
                        <ul class="sub">
                            <li><a href="{{ url('/backoffice/products') }}">Alle
                                    producten</a></li>
                            <li>
                                <a href="{{ url('/backoffice/products/subpages/deleted') }}">Verwijderd</a>
                            </li>
                            <li>
                                <a href="{{ url('/backoffice/products/subpages/inactive') }}">Inactief</a>
                            </li>
                            <li>
                                <a href="{{ url('/backoffice/products_lend') }}">Uitgeleende</a>
                            </li>
                            <li>
                                <a href="{{ url('/backoffice/products_reserved') }}">Gereserveerd</a>
                            </li>
                            <li>
                                <a href="{{ url('/backoffice/products_not_reserved') }}">Niet
                                    uitgeleend</a></li>
                            <li>
                                <a href="{{ url('/backoffice/products_not_lend') }}">Niet
                                    gereserveerd</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="#" {{ (Request::is('*reservations*') ? 'class=active' : '') }}>
                            <i class="fa fa-tasks"></i>
                            <span>Reservaties</span>
                        </a>
                        <ul class="sub">
                            <li>
                                <a href="{{ url('/backoffice/reservations') }}">Alle
                                    reservaties</a></li>
                            <li><a href="{{ url('/backoffice/todo') }}">Nog af te
                                    handelen</a></li>
                            <li>
                                <a href="{{ url('/backoffice/reservations_planned') }}">Geplande
                                    reservaties</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ url('/backoffice/calendar') }}" {{ (Request::is('*calendar*') ? 'class=active' : '') }}>
                            <i class="fa fa-calendar"></i>
                            <span>Calender</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/backoffice/suppliers') }}" {{ (Request::is('*suppliers*') ? 'class=active' : '""') }}>
                            <i class="fa fa-comments-o"></i>
                            <span>Leveranciers</span>
                        </a>
                    </li>
                @endif
            </ul>
            <!-- sidebar menu end-->
        </div>
    </aside>
    <!--sidebar end-->

    <!-- **********************************************************************************************************************************************************
    MAIN CONTENT
    *********************************************************************************************************************************************************** -->
    <!--main content start-->
    <section id="main-content">

            @yield('content')

            <! --/container -->
    </section>
    <!-- /MAIN CONTENT -->

    <!--main content end-->
    <!--footer start-->
    <footer class="site-footer">
        <div class="text-center">
            2016 - Neeltje Chanterie
            <a href="#" class="go-top">
                <i class="fa fa-angle-up"></i>
            </a>
        </div>
    </footer>
    <!--footer end-->
</section>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
{{-- <script src="{{ elixir('backoffice/js/app.js') }}"></script> --}}

        <!-- js placed at the end of the document so the pages load faster -->
<script src="{{ URL::asset('backoffice/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/jquery.dcjqaccordion.2.7.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('backoffice/js/jquery.sparkline.js') }}"></script>


<!-- Dependencies -->
<script src="{{URL::asset('bower_components/angular/angular.js')}}"></script>
<script src="{{URL::asset('bower_components/angular-bootstrap/ui-bootstrap.js')}}"></script>
<script src="{{URL::asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.js')}}"></script>


<!-- Our Application Scripts -->
<script src="{{ URL::asset('backoffice/js/app.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/AirplanesCtrl.js') }}"></script>

<!--common script for all pages-->
<script src="{{ URL::asset('backoffice/js/common-scripts.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/sparkline-chart.js') }}"></script>

<script src="{{ URL::asset('backoffice/js/calendar-conf-events.js') }}"></script>



</body>
</html>
