<!DOCTYPE html>
<html lang="en" ng-app="App">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Uitleenbalie - Backoffice</title>


    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('backoffice/css/bootstrap.css') }}" rel="stylesheet">
    <!--external css-->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/zabuto_calendar.css') }}">
    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('backoffice/css/main.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('backoffice/css/style-responsive.css') }}" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
            rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>

</head>
<body id="app-layout">

<div id="login-page">
    <div class="container">

            @yield('content')

    </div>
</div>

<!-- JavaScripts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
{{--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>--}}
{{-- <script src="{{ elixir('backoffice/js/app.js') }}"></script> --}}

        <!-- js placed at the end of the document so the pages load faster -->
<script src="{{ URL::asset('backoffice/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/jquery.dcjqaccordion.2.7.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/jquery.nicescroll.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('backoffice/js/jquery.sparkline.js') }}"></script>


<!-- Dependencies -->
<script src="{{URL::asset('bower_components/angular/angular.js')}}"></script>
<script src="{{URL::asset('bower_components/angular-bootstrap/ui-bootstrap.js')}}"></script>
<script src="{{URL::asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.js')}}"></script>


<!-- Our Application Scripts -->
<script src="{{ URL::asset('backoffice/js/app.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/AirplanesCtrl.js') }}"></script>

<!--common script for all pages-->
<script src="{{ URL::asset('backoffice/js/common-scripts.js') }}"></script>
<script src="{{ URL::asset('backoffice/js/sparkline-chart.js') }}"></script>

</body>
</html>
