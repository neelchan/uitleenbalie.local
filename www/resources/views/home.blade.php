@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height">
    <div class="container">
        <div class="row">
            <div class="panel-body">
                <div class="row mt">
                    <div class="col-lg-4 col-md-4 col-sm-4 mb">
                        <div class="twitter-panel pn">
                            <i class="fa fa-heart fa-4x"></i>
                            <p>Aantal gebruikers</p>
                            <p style="font-size: 50pt">{{ $userStats['total'] }}</p>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 mb">
                        <div class="white-panel pn">
                            <div class="white-header">
                                <h5>NIEUWSTE GEBRUIKER</h5>
                            </div>
                            <p>
                                <img src="https://d13yacurqjgara.cloudfront.net/users/124355/screenshots/2199042/profile.png"
                                     class="img-circle" height="100"></p>
                            <p><b>{{ $userStats['newest'] }}</b></p>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="small mt">MEMBER SINCE</p>
                                    <p>{{ $userStats['newestDate'] }}</p>
                                </div>
                                <div class="col-md-6">
                                    <p class="small mt">E-MAIL</p>
                                    <p>{{ $userStats['newestEmail'] }}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 mb">
                        <div class="twitter-panel pn">
                            <i class="fa fa-heart fa-4x"></i>
                            <p>Aantal producten</p>
                            <p style="font-size: 50pt">{{ $productStats['total'] }}</p>
                        </div>
                    </div>
                </div>
                <div class="row mt">
                    @foreach($latestProducts as $index => $product)
                    <div class="col-lg-4 col-md-4 col-sm-4 mb">
                        <div class="white-panel pn">
                            <div class="white-header">
                                <h5>NIEUWSTE PRODUCTEN</h5>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-xs-12 goleft">
                                    <p>{{$index + 1}} <i class="fa fa-heart"></i> {{ $product->name }}</p>
                                </div>
                                <div class="col-sm-6 col-xs-6"></div>
                            </div>
                            <div class="centered">
                                <a href="products/{{$product->id}}">
                                <img src="/uploads/products/{{ $product->image }}" style="max-height: 150px;" id="{{ $product->id }}">
                                </a>
                            </div>
                        </div>
                    </div><!-- /col-md-4 -->
                    @endforeach
                </div>

                <div class="row mt">
                    <div class="col-lg-12 col-md-12 col-sm-12 mb">
                    <!--CUSTOM CHART START -->
                    {{--<div class="border-head">--}}
                        <div class="white-panel pn" style="height:425px;">
                            <div class="white-header">
                                <h5>GEBRUIKERS</h5>
                            </div>
                    {{--</div>--}}
                            <div class="col-md-10 col-md-offset-1">
                                <div class="custom-bar-chart">
                                    <ul class="y-axis">
                                        <li><span>{{ $userStats['total'] }}</span></li>
                                        <li><span>{{ $userStats['total']/1.3333333333333333 }}</span></li>
                                        <li><span>{{ $userStats['total']/2 }}</span></li>
                                        <li><span>{{ $userStats['total']/4 }}</span></li>
                                        <li><span>{{ $userStats['total']/8 }}</span></li>
                                        <li><span>0</span></li>
                                    </ul>
                                <div class="bar ">
                                    <div class="title">Inactief</div>
                                    <div class="value tooltips" data-original-title="{{ $userStats['inactive'] }}" data-toggle="tooltip" data-placement="top">{{ $userStats['inactive']/$userStats['total']*100 }}%</div>
                                </div>
                                <div class="bar ">
                                    <div class="title">Actief</div>
                                    <div class="value tooltips" data-original-title="{{ $userStats['active'] }}" data-toggle="tooltip" data-placement="top">{{ $userStats['active']/$userStats['total']*100 }}%</div>
                                </div>
                                <div class="bar ">
                                    <div class="title">Disabled</div>
                                    <div class="value tooltips" data-original-title="{{ $userStats['banned'] }}" data-toggle="tooltip" data-placement="top">{{ $userStats['banned']/$userStats['total']*100 }}%</div>
                                </div>
                                <div class="bar">
                                    <div class="title">Disabled & actief</div>
                                    <div class="value tooltips" data-original-title="{{ $userStats['bannedButActive'] }}" data-toggle="tooltip" data-placement="top">{{ $userStats['bannedButActive']/$userStats['total']*100 }}%</div>
                                </div>
                                <div class="bar ">
                                    <div class="title">Totaal</div>
                                    <div class="value tooltips" data-original-title="{{ $userStats['total'] }}" data-toggle="tooltip" data-placement="top">{{ $userStats['total']/$userStats['total']*100 }}%</div>
                                </div>
                                </div>
                            </div>
                    <!--custom chart end-->
                        </div>
                    </div><!-- /row -->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
