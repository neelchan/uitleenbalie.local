@extends('layouts.frontoffice')

@section('content')
    <section id="catalogus-detail">
        <div class="container">
            <div class="breadcrumb text-right">
                <a href="{{ url('frontoffice/home') }}">HOME</a> / <a href="{{ url('frontoffice/catalogus') }}">CATALOGUS</a> / <a href="#">ALL</a> / <a
                        href="#">{{$product->name}}</a>
            </div>
            <div class="catalogus-detail col-xs-12">
                <div class="col-sm-6 col-xs-12 no-padding container-detail">
                    <img src="/uploads/products/{{ $product->image }}" class="img-responsive" alt="">
                </div>

                <div class="col-sm-6 col-xs-12 container-detail-content">
                    <h3 class="ruler">
                        {{$product->name}}
                    <span class="right">
                        <i class="fa fa-calendar-check-o green"></i>
                        <span class="icon">
                            <span class="favo">
                                <span class="heart"></span>
                            </span>
                        </span>
                    </span>

                    </h3>
                    <div class="row">
                        <a href="#" class="btn btn-primary col-lg-5 col-xs-12">Huren</a>
                        <a href="#" class="btn btn-primary col-lg-5 col-xs-12 pull-right">Reserveren</a>
                    </div>
                    <br>
                    <p>{{ $product->description }}</p>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <h3 class="ruler-top">Specificaties product</h3>
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th>Naam</th>
                            <td>{{ $product->name }}</td>
                        </tr>
                        <tr>
                            <th>Merk</th>
                            <td>{{ $product->brand }}</td>
                        </tr>
                        <tr>
                            <th>Jaar aankoop</th>
                            <td>2014</td>
                        </tr>
                        <tr>
                            <th>Groepsnummer</th>
                            <td>24</td>
                        </tr>
                        <tr>
                            <th>Product type</th>
                            <td>{{ $product->type }}</td>
                        </tr>
                        <tr>
                            <th>Groep</th>
                            <td>Kit</td>
                        </tr>
                        <tr>
                            <th>Code</th>
                            <td>{{ $product->product_code }}</td>
                        </tr>
                        <tr>
                            <th>Tags</th>
                            <td>{{ $product->tags }}</td>
                        </tr>
                        <tr>
                            <th>Status</th>
                            <td>
                                @if($product->active == 1)
                                    <span class="label label-success">Active</span>
                                @else
                                    <span class="label label-default">Inactive</span>
                                @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="row">
                    <h3 class="ruler-top">Gemiddelde beoordeling</h3>
                    <br>
                    <span class="rating">
                        <span class="star"></span><span class="star"></span><span class="star"></span><span
                                class="star"></span><span class="star"></span>
                    </span>
                    <span class="right">9 beoordelingen</span>
                </div>
                <div class="row">
                    <h3>Recente beoordelingen</h3>
                    <table class="comment">
                        <th class="block">Zeer handige reistas</th>
                        <td class="block">Aangezien ik deze kit nodig had buiten de schoolomgeving, was het zeer handig
                            dat deze een reiskoffer had.
                        </td>
                    </table>
                    <hr class="short">

                    <table class="comment">
                        <th class="block">Goede belichting</th>
                        <td class="block">Door deze set heb ik super mooie foto's kunnen maken met mooie belichting!
                        </td>
                    </table>
                    <hr class="short">

                    <table class="comment">
                        <th class="block">Handig in elkaar te steken</th>
                        <td class="block">Deze kit is zeer handig om overal mee te nemen. In een mum van tijd staat hij
                            opgesteld.
                        </td>
                    </table>
                    <hr class="short">

                    <br>
                    <a href="#">Lees meer ...</a>
                </div>

            </div>
            <div class="col-xs-12 related-product">
                <h3 class="ruler-top">Related products</h3>
                <div class="col-md-3 col-sm-6 col-xs-12 product">
                    <div class="content">
                        <a href="detail.html" class="portfolio-box">
                            <div class="product-image-related">
                                <img src="http://cdn01.androidauthority.net/wp-content/uploads/2014/08/canon-5d-mk3.jpg"
                                     class="img-responsive" alt="">
                            </div>
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Canon EOS 5D Mark III
                                    </div>
                                    <div class="product-name">
                                        HUUR
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 product">
                    <div class="content">
                        <a href="detail.html" class="portfolio-box">
                            <div class="product-image-related">
                                <img src="http://fiilex.com/images/product/kits/K302_content.jpg"
                                     class="img-responsive" alt="">
                            </div>
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Fiilex K301 Lighting Kit
                                    </div>
                                    <div class="product-name">
                                        HUUR
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 product">
                    <div class="content">
                        <a href="detail.html" class="portfolio-box">
                            <div class="product-image-related">
                                <img src="http://filmbewerking.nl/wp-content/uploads/2014/09/green-screen.jpg"
                                     class="img-responsive" alt="">
                            </div>
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Green screen
                                    </div>
                                    <div class="product-name">
                                        HUUR
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-12 product">
                    <div class="content">
                        <a href="detail.html" class="portfolio-box">
                            <div class="product-image-related">
                                <img src="https://www.thephonestore.be/wp-content/uploads/2015/09/go-pro-hero-4-zwart.jpg"
                                     class="img-responsive" alt="">
                            </div>
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        GOPRO HERO 4 SILVER
                                    </div>
                                    <div class="product-name">
                                        HUUR
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
