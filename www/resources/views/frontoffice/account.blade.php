@extends('layouts.frontoffice')

@section('content')
    <section id="account">
        <div class="container">
            <!--<div class="breadcrumb text-right">-->
            <!--<a href="index.html">HOME</a> / <a href="catalogus.html">CATALOGUS</a> / <a href="#">ALL</a> / <a-->
            <!--href="detail.html">PRODUCT NAME</a>-->
            <!--</div>-->
            <div class="catalogus-detail">
                <div class="col-sm-3 col-xs-12 no-padding container-detail">
                    @if( Auth::user()->image == null)
                    <img src="https://d13yacurqjgara.cloudfront.net/users/124355/screenshots/2199042/profile.png"
                         class="img-responsive" alt="">
                    @else
                        <img src="/uploads/users/{{Auth::user()->image}}"
                             class="img-responsive">
                    @endif
                </div>






                <div class="col-sm-9 col-xs-12 container-detail-content">
                    @if( Auth::user()->last_name == null && Auth::user()->first_name == null)
                        <h3 class="ruler"><a href="/account/{!! Auth::user()->id !!}/edit" aria-expanded="false">
                                <i class="fa fa-pencil"></i> Bewerk je profiel
                            </a></h3>
                    @else
                        <h3 class="ruler">
                            {{Auth::user()->first_name}} {{Auth::user()->last_name}}
                            <span class="pull-right">
                                <a href="/account/{!! Auth::user()->id !!}/edit" class="btn btn-theme03">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                <a href="/account/{!! Auth::user()->id !!}/editImage"
                                   class="btn btn-theme02"><i class="fa fa-camera-retro"></i>
                                </a>
                            </span>
                        </h3>

                    @endif
                    <h4>Informatie:</h4>
                    <div class="table-responsive">
                        <table class="table ">
                            <tr>
                                <th>Familienaam</th>
                                @if( Auth::user()->last_name == null)
                                    <td><a
                                       href="/account/{!! Auth::user()->id !!}/edit"
                                       aria-expanded="false">
                                        <i class="fa fa-pencil"></i>
                                    </a></td>
                                @else
                                    <td>{{ Auth::user()->last_name }}</td>
                                @endif
                            </tr>
                            <tr>
                                <th>Voornaam</th>
                                @if( Auth::user()->first_name == null)
                                    <td><a
                                                href="/account/{!! Auth::user()->id !!}/edit"
                                                aria-expanded="false">
                                            <i class="fa fa-pencil"></i>
                                        </a></td>
                                @else
                                    <td>{{ Auth::user()->first_name }}</td>
                                @endif
                            </tr>
                            <tr>
                                <th>Gebruikersnaam</th>
                                <td>{{ Auth::user()->name }}</td>
                            </tr>
                            <tr>
                                <th>Studentennummer</th>
                                <td>{{ Auth::user()->student_number }}</td>
                            </tr>
                            <tr>
                                <th>E-mail</th>
                                <td>{{ Auth::user()->email }}</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="account-items">
                <div class="container col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading">Jouw leningen & reservaties</h2>
                            <hr class="orange">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <h3 class="ruler-top">Aangevraagde producten</h3>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Titel</th>
                                <th>Vrij?</th>
                                <th>Niet beschikbaar tot</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Camera</td>
                                <td><i class="fa fa-clock-o"></i></td>
                                <td>02/02/2016</td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Standaard</td>
                                <td><i class="fa fa-check"></i></td>
                                <td>-</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-sm-6 col-xs-12">
                    <h3 class="ruler-top">Producten in bezit</h3>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Titel</th>
                                <th>In bezit tot</th>
                                <th>Te laat terug?</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Camera</td>
                                <td>02/02/2016</td>
                                <td><i class="fa fa-frown-o"></i></td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Standaard</td>
                                <td>03/04/2016</td>
                                <td><i class="fa fa-smile-o"></i></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="account-favo">
                <div class="container col-xs-12">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <h2 class="section-heading">Favorieten</h2>
                            <hr class="pink">
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 favos">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Titel</th>
                                <th>Afbeelding</th>
                                <th>Merk</th>
                                <th>Product type</th>
                                <th>Groep</th>
                                <th>Code</th>
                                <th>Tags</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Camera</td>
                                <td><img src="http://fiilex.com/images/product/kits/K302_content.jpg"
                                         class="img-responsive" alt=""></td>
                                <td>Fiilex</td>
                                <td>AVM</td>
                                <td>Kit</td>
                                <td>1234567890</td>
                                <td>Filmmateriaal, belichting</td>
                                <td><i class="fa fa-calendar-check-o"></i></td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Camera</td>
                                <td><img src="http://filmbewerking.nl/wp-content/uploads/2014/09/green-screen.jpg"
                                         class="img-responsive" alt=""></td>
                                <td>Filmbewerking</td>
                                <td>AVM</td>
                                <td>Kit</td>
                                <td>1234567890</td>
                                <td>Filmen</td>
                                <td><i class="fa fa-calendar-check-o"></i></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
