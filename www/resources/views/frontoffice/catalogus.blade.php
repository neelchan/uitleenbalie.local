@extends('layouts.frontoffice')

@section('content')
    <section id="catalogus">
        <div class="container">
            <div class="row sort-row">
                <div class="btn-group col-xs-10" role="group" aria-label="...">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Categorie
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="#">Dropdown link</a></li>
                            <li><a href="#">Dropdown link</a></li>
                        </ul>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Merk
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            {{--<li><a href="#">Dropdown link</a></li>--}}
                            {{--<li class="divider"></li>--}}
                            {{--<li class="dropdown-header">Merk</li>--}}
                            @foreach ($products_brand as $name => $brand)
                                <li><a href="/catalogus?term={{ $brand }}">{{ $brand }}</a></li>
                            @endforeach
                        </ul>
                    </div>



                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                            Type
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            @foreach ($products_type as $name => $type)
                                <li><a href="/catalogus?term={{ $type }}">{{ $type }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                </div>
                <div class="btn-group pull-right col-xs-2">
                    <button type=button class="btn btn-default dropdown-toggle pull-right" data-toggle=dropdown aria-haspopup=true
                            aria-expanded=false>Sorteren op <span class=caret></span></button>
                    <ul class=dropdown-menu>
                        <li><a href=#>Beschikbaar</a></li>
                        <li><a href=#>Niet beschikbaar</a></li>

                    </ul>
                </div>
            </div>

            @foreach ($products as $product)
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 product">
                <div class="content">
                    <a href="catalogus/{{$product->id}}" class="portfolio-box">
                        <div class="product-image">
                            @if($product->active == 1)
                                <span class="top green"><i class="fa fa-calendar-check-o"></i> Beschikbaar</span>
                            @else
                                <span class="top pink"><i class="fa fa-clock-o"></i> Niet beschikbaar</span>
                            @endif
                            <img src="/uploads/products/{{ $product->image }}"
                                 class="img-responsive" alt="">
                        </div>
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="product-name">
                                    RESERVEER
                                </div>
                            </div>
                        </div>
                    </a>
                    <div class="description">
                        <h3>{{ $product->name }}</h3>
                        <!--<span class="right"><a href="#" data-toggle="tooltip" data-placement="right" title="Niet beschikbaar"><i class="fa fa-clock-o faa-parent animated-hover pink"></i></a></span>-->
                        <p>
                            {{--{{ $product->description }}--}}
                        short description
                        </p>
                        <p><i class="fa fa-tag"></i> {{ $product->tags }} </p>
                    </div>
                </div>
            </div>
            @endforeach

            <div class="col-md-12 text-center">
                {!! $products->appends(\Input::except('page'))->render() !!}
            </div>

            {{--<nav class="col-xs-12">--}}
                {{--<ul class="pager">--}}
                    {{--<li class="previous disabled"><a href="#"><span aria-hidden="true">&larr;</span> Older</a></li>--}}
                    {{--<li><a href="#">1</a></li>--}}
                    {{--<li><a href="#">2</a></li>--}}
                    {{--<li><a href="#">3</a></li>--}}
                    {{--<li><a href="#">4</a></li>--}}
                    {{--<li><a href="#">5</a></li>--}}
                    {{--<li class="next"><a href="#">Newer <span aria-hidden="true">&rarr;</span></a></li>--}}
                {{--</ul>--}}
            {{--</nav>--}}
        </div>
    </section>
@endsection
