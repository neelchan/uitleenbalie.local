@extends('layouts.frontoffice')

@section('content')
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Let's Get In Touch!</h2>
                    <hr class="primary">
                    <p>Zit je met een vraag? Of heb je problemen met de app? Laat het ons weten!</p>
                </div>

                <div class="container">
                    <div class="row">
                        <form role="form" action="" method="post">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="InputName">Naam</label>
                                    <input type="text" class="form-control" name="InputName" id="InputName"
                                           placeholder="Enter Name" required>
                                </div>
                                <div class="form-group">
                                    <label for="InputEmail">E-Mail</label>
                                    <input type="email" class="form-control" id="InputEmail" name="InputEmail"
                                           placeholder="Enter Email" required>
                                </div>
                                <div class="form-group">
                                    <label for="InputMessage">Bericht</label>
                                    <textarea name="InputMessage" id="InputMessage" class="form-control" rows="3" required></textarea>
                                </div>
                                <input type="submit" name="submit" id="submit" value="Submit"
                                       class="btn btn-primary pull-right">
                            </div>
                        </form>
                        <hr class="featurette-divider hidden-lg">
                        <div class="col-lg-5 col-md-push-1">
                            <address>
                                <h3>Arteveldehogeschool Campus Mariakerke</h3>
                                <p>
                                    <i class="fa fa-phone wow bounceIn"> </i>
                                    09 234 86 00
                                </p>
                                <p>
                                    <i class="fa fa-envelope-o wow bounceIn" data-wow-delay=".1s"> </i>
                                    <a href="mailto:krisra@arteveldehs.com">krisra@arteveldehs.com</a>
                                </p>

                                <p>
                                    <i class="fa fa-map-marker wow bounceIn"></i>
                                    Mediacampus Mariakerke <br>
                                    Industrieweg 232 <br>
                                    9030 Gent-Mariakerke <br>
                                </p>
                            </address>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection
