@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height">
    <div class="row content-detail">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Nieuwe gebruiker</div>
                <div class="panel-body">

                    <div class="col-md-10 col-md-offset-1">

                        {!! Form::open(['url' => 'backoffice/users', 'files' => 'true', 'class' => 'form-horizontal']) !!}

                        <div class="form-group">
                            {!! Form::label('name', 'Gebruikersnaam:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Voornaam:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Achternaam:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Studentennummer:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('student_number', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Groep:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('group', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'E-mail:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'Wachtwoord:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('password', 'Herhaal wachtwoord:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('image', 'Afbeelding', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::file('image', ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-4">
                                {!! Form::hidden('active', 0) !!}
                                {!! Form::checkbox('active', 1, null, ['class' => 'col-md-1 field']) !!}
                                {!! Form::label('active', 'Actief') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-4">
                                {!! Form::hidden('disabled', 0) !!}
                                {!! Form::checkbox('disabled', 1, null, ['class' => 'col-md-1 field'])  !!}
                                {!! Form::label('disabled', 'Geblokkeerd') !!}
                            </div>
                        </div>

                        {!! Form::submit('Submit', ['class' => 'btn btn-theme03 pull-right']) !!}
                        <a href="{{ URL::previous() }}" class="btn btn-theme04">Back</a>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection