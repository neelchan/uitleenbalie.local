@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height">
    <div class="row content-detail">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Edit {!! $user->name !!}</div>
                <div class="panel-body">

                    <div class="col-md-10 col-md-offset-1">

                        {!! Form::model($user, ['method' => 'POST', 'action' => ['UsersController@updateUser', $user->id], 'class' => 'form-horizontal']) !!}
                        <div class="form-group">
                            {!! Form::label('name', 'Gebruikersnaam:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('first_name', 'Voornaam:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('last_name', 'Achternaam:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('student_number', 'Studentennummer:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('student_number', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('group', 'Studentengroep:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('group', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('email', 'E-mail:', ['class' => 'col-md-4 control-label']) !!}
                            <div class="col-md-8">
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-4">
                                {!! Form::hidden('active', 0) !!}
                                {!! Form::checkbox('active', 1, null, ['class' => 'col-md-1 field']) !!}
                                {!! Form::label('active', 'Actief') !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-4">
                                {!! Form::hidden('disabled', 0) !!}
                                {!! Form::checkbox('disabled', 1, null, ['class' => 'col-md-1 field'])  !!}
                                {!! Form::label('disabled', 'Geblokkeerd') !!}
                            </div>
                        </div>

                        {!! Form::submit('Submit', ['class' => 'btn btn-primary pull-right']) !!}
                        <a href="{{ URL::previous() }}" class="btn btn-theme04">Back</a>

                        {!! Form::close() !!}
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
@endsection