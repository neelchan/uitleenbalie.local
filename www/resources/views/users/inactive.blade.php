@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div class="row content-panel">
                <div id="all" class="tab-pane active">
                    <div class="col-md-12 detailed">
                        <div class="table-responsive">
                            <h2>Inactieve gebruikers
                                            <span class="pull-right">
                                                <a href="/users/create" class="btn btn-theme03">Nieuwe gebruiker</a>
                                            </span>
                            </h2>
                            <div class="search">

                                {{--{{ Form::model(null, array('route' => array('users.search'))) }}--}}
                                {{--{{ Form::text('query', null, array( 'placeholder' => 'Search query...' )) }}--}}
                                {{--{{ Form::submit('Search') }}--}}
                                {{--{{ Form::close() }}--}}


                            </div>

                            {{--<table class="table">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                            {{--<th>@sortablelink ('id')</th>--}}
                            {{--<th>@sortablelink ('name')</th>--}}
                            {{--<th>E-mail</th>--}}
                            {{--<th>Created</th>--}}
                            {{--<th>Updated</th>--}}
                            {{--<th>Actions</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}

                            {{--@foreach ($users as $user)--}}
                            {{--<tr>--}}
                            {{--<td>{{$user->id}}</td>--}}
                            {{--<td>{{$user->name}}</td>--}}
                            {{--<td>{{$user->email}}</td>--}}
                            {{--<td>{{$user->created_at}}</td>--}}
                            {{--<td>{{$user->updated_at}}</td>--}}
                            {{--<td><a href="users/{{$user->id}}"><i class="fa fa-eye"></i></a></td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                            {{--</tbody>--}}
                            {{--</table>--}}
                            {{--<div class="col-md-12 text-center">--}}
                            {{--{!! $users->appends(\Input::except('page'))->render() !!}--}}
                            {{--</div>--}}

                            @if ($users->count())
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            @if ($sortby == 'id' && $order == 'asc') {{ link_to_action(
                                                            'UsersController@index',
                                                            '#',
                                                            array(
                                                            'sortby' => 'id',
                                                            'order' => 'desc'
                                                        )) }} <i class="fa fa-sort-numeric-asc"></i>
                                            @else {{ link_to_action(
                                                        'UsersController@index',
                                                        '#',
                                                        array(
                                                            'sortby' => 'id',
                                                            'order' => 'asc'
                                                        )) }} <i class="fa fa-sort-numeric-desc"></i>
                                            @endif
                                        </th>
                                        <th>
                                            @if ($sortby == 'name' && $order == 'asc') {{ link_to_action(
                                                            'UsersController@index',
                                                            'Naam',
                                                            array(
                                                            'sortby' => 'name',
                                                            'order' => 'desc'
                                                        )) }} <i class="fa fa-sort-alpha-asc"></i>
                                            @else {{ link_to_action(
                                                        'UsersController@index',
                                                        'Naam',
                                                        array(
                                                            'sortby' => 'name',
                                                            'order' => 'asc'
                                                        )) }} <i class="fa fa-sort-alpha-desc"></i>
                                            @endif
                                        </th>
                                        <th>E-mail</th>
                                        <th>Status</th>
                                        <th>Aangemaakt</th>
                                        <th>Aangepast</th>
                                        <th>Acties</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{ $user->id }}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{$user->email}}</td>
                                            <td>@if($user->active == 1 && $user->disabled == 0)
                                                    <span class="label label-success">Active</span>
                                                @endif
                                                @if($user->disabled == 1)
                                                    <span class="label label-danger">Banned</span>
                                                @endif
                                                @if($user->active == 0 && $user->disabled == 0)
                                                    <span class="label label-default">Inactive</span>
                                                @endif
                                                @if($user->active == 1 && $user->disabled == 1)
                                                    <span class="label label-default">Still active!</span>
                                                @endif</td>
                                            <td>{{$user->created_at->format('m/d/Y')}}</td>
                                            <td>{{$user->updated_at->format('m/d/Y')}}</td>
                                            <td style="width: 11.45%">
                                                <a href="../{{$user->id}}" class="btn btn-default"
                                                   aria-expanded="false">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a class="btn btn-default"
                                                   href="/users/{!! $user->id !!}/edit"
                                                   aria-expanded="false">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <div aria-expanded="false" class="pull-right">
                                                    {!! Form::model($user, ['method' => 'DELETE', 'action' => ['UsersController@softDelete', $user->id], 'onsubmit' => 'return AlertDelete()']) !!}
                                                    {!! Form::button('<i class="fa fa-ban"></i>', array(
                                                    'type' => 'submit',
                                                    'aria-expanded' => 'false',
                                                    'class'=> 'btn btn-default',
                                                    'onclick'=>'return confirm("Are you sure you want to delete this?")'
                                                    )) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="col-md-12 text-center">
                                    {!! $users->links() !!}
                                </div>
                            @else
                                There are no inactive users
                            @endif
                        </div>
                        {{--table-responsive--}}
                    </div>
                    {{--col-md-12 detailed--}}
                </div>
                {{--tab-panel--}}
            </div>
            {{--/row content-panel--}}
        </div>
        {{--/col-lg-12--}}
    </div>
    {{--/row--}}
</section>
@endsection
