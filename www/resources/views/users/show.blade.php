@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div class="row content-panel">
                <div class="col-xs-12">
                <a href="{{ URL::previous() }}" class="btn btn-theme03"><i class="fa fa-arrow-left"></i> Back</a>
                <hr>
                </div>
                <div class="col-md-4 profile-text mb centered">
                    <div class="right-divider">
                        <div class="profile-pic">
                            {{--<img src="https://d13yacurqjgara.cloudfront.net/users/124355/screenshots/2199042/profile.png"--}}
                                 {{--class="img-circle">--}}
                            <img src="/uploads/users/{{$user->image}}"
                                 class="img-circle">
                        </div>
                    </div>
                </div>
                <! --/col-md-4 -->

                <div class="col-md-8 profile-text">

                    <h3>{{$user->first_name}} {{$user->last_name}}
                        <span class="pull-right">
                            <a href="{!! $user->id !!}/edit" class="btn btn-theme03">
                                <i class="fa fa-pencil"></i>
                            </a>
                            {{--<a href="users/{!! $user->id !!}/editImage"--}}
                               {{--class="btn btn-theme02"><i class="fa fa-camera-retro"></i>--}}
                            {{--</a>--}}

                            <a href="/backoffice/users/{!! $user->id !!}/editImage"
                               class="btn btn-theme02"><i class="fa fa-camera-retro"></i>
                            </a>
                            <div aria-expanded="false" class="pull-right ml">
                                {!! Form::model($user, ['method' => 'DELETE', 'action' => ['UsersController@softDelete', $user->id], 'onsubmit' => 'return AlertDelete()']) !!}
                                {!! Form::button('<i class="fa fa-ban"></i>', array(
                                'type' => 'submit',
                                'aria-expanded' => 'false',
                                'class'=> 'btn btn-theme04',
                                'onclick'=>'return confirm("Are you sure you want to delete this?")'
                                )) !!}
                                {!! Form::close() !!}
                            </div>
                        </span>
                    </h3>
                    <h6>{{$user->name}} | {{$user->email}} </h6>
                    <table class="table">
                        <tr>
                            <th class="col-xs-5">ID</th>
                            <td class="col-xs-7">{{$user->id}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Studentennummer</th>
                            <td class="col-xs-7">{{$user->student_number}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Studentengroep</th>
                            <td class="col-xs-7">{{$user->group}}</td>
                        </tr>
                        <tr>
                            <th class="col-xs-5">Status</th>
                            <td>@if($user->active == 1 && $user->disabled == 0)
                                    <span class="label label-success">Active</span>
                                @endif
                                @if($user->disabled == 1)
                                    <span class="label label-danger">Banned</span>
                                @endif
                                @if($user->active == 0 && $user->disabled == 0)
                                    <span class="label label-default">Inactive</span>
                                @endif
                                @if($user->active == 1 && $user->disabled == 1)
                                    <span class="label label-default">Still active!</span>
                                @endif</td>
                        </tr>
                    </table>
                    {{--<table class="table">--}}
                        {{--<thead>--}}
                        {{--<tr>--}}
                            {{--<th>Code</th>--}}
                            {{--<th>Barcode</th>--}}
                            {{--<th>QR-code</th>--}}
                            {{--<th>Aangemaakt</th>--}}
                            {{--<th>Aangepast</th>--}}
                            {{--@if($product->deleted_at != null)--}}
                                {{--<th>Verwijderd</th>--}}
                            {{--@endif--}}
                        {{--</tr>--}}
                        {{--</thead>--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                            {{--<td>{{$product->product_code}}</td>--}}
                            {{--<td>{{$product->barcode}}</td>--}}
                            {{--<td>{{$product->QR_code}}</td>--}}
                            {{--<td>{{ $product->created_at->format('d/m/Y H:i')}}</td>--}}
                            {{--<td>{{$product->updated_at->format('d/m/Y H:i')}}</td>--}}
                            {{--@if($product->deleted_at != null)--}}
                                {{--<td>{{$product->deleted_at->format('d/m/Y H:i')}}</td>--}}
                            {{--@endif--}}

                        {{--</tr>--}}
                        {{--</tbody>--}}
                    {{--</table>--}}
                </div>
                <! --/col-md-4 -->


            </div><!-- /row -->
        </div>
        <! --/col-lg-12 -->

        <div class="col-lg-12 mt">
            <div class="row content-panel">
                <div class="panel-heading">
                    <ul class="nav nav-tabs nav-justified">
                        <li class="active">
                            <a data-toggle="tab" href="profile.html#overview">Overzicht</a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="profile.html#contact" class="contact-map">Gegevens</a>
                        </li>
                    </ul>
                </div>
                <! --/panel-heading -->

                <div class="panel-body">
                    <div class="tab-content">
                        <div id="overview" class="tab-pane active">
                            <div class="row">
                                <div class="col-md-6 detailed">
                                    <h4>Status gebruiker</h4>
                                    <div class="row centered mt mb">
                                        <div class="col-sm-4">
                                            <h1><i class="fa fa-database"></i></h1>
                                            <h3>2</h3>
                                            <h6>Producten in bezit</h6>
                                        </div>
                                        <div class="col-sm-4">
                                            <h1><i class="fa fa-trophy"></i></h1>
                                            <h3>10</h3>
                                            <h6>Producten reeds geleend en teruggebracht</h6>
                                        </div>
                                        <div class="col-sm-4">
                                            <h1><i class="fa fa-cart-arrow-down"></i></h1>
                                            <h3>1</h3>
                                            <h6>Product te laat teruggebracht</h6>
                                        </div>

                                    </div><!-- /row -->
                                </div>
                                <!-- /col-md-6 -->
                                <div class="col-md-6 detailed">
                                    <h4>Reservaties</h4>
                                    <!-- CALENDAR-->
                                    <div id="calendar" class="mb">
                                        <div class="panel green-panel no-margin">
                                            <div class="panel-body">
                                                <div id="date-popover" class="popover top"
                                                     style="cursor: pointer; disadding: block; margin-left: 33%; margin-top: -50px; width: 175px;">
                                                    <div class="arrow"></div>
                                                    <h3 class="popover-title" style="disadding: none;"></h3>
                                                    <div id="date-popover-content"
                                                         class="popover-content"></div>
                                                </div>
                                                <div id="my-calendar"></div>
                                            </div>
                                        </div>
                                    </div><!-- / calendar -->
                                </div>
                                <! --/col-md-6 -->
                            </div>
                            <! --/OVERVIEW -->
                        </div>
                        <! --/tab-pane -->

                        <div id="contact" class="tab-pane">
                            <div class="row">
                                <div class="col-md-12 detailed">
                                    <div class="table-responsive">
                                        <h2>{{$user->name}}</h2>

                                        <table class="table ">
                                            <tr>
                                                <th class="col-xs-5">Gebruikersnaam</th>
                                                <td class="col-xs-7">{{$user->name}}</td>
                                            </tr>
                                            <tr>
                                                <th class="col-xs-5">Voornaam</th>
                                                <td class="col-xs-7">{{$user->first_name}}</td>
                                            </tr>
                                            <tr>
                                                <th class="col-xs-5">Achternaam</th>
                                                <td class="col-xs-7">{{$user->last_name}}</td>
                                            </tr>
                                            <tr>
                                                <th class="col-xs-5">Studentennummer</th>
                                                <td class="col-xs-7">{{$user->student_number}}</td>
                                            </tr>
                                            <tr>
                                                <th class="col-xs-5">Studentengroep</th>
                                                <td class="col-xs-7">{{$user->group}}</td>
                                            </tr>
                                            <tr>
                                                <th>E-mail</th>
                                                <td>{{$user->email}}</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <! --/col-md-12 -->
                            </div>
                            <! --/row -->
                        </div>
                        <! --/tab-pane -->
                    </div><!-- /tab-content -->

                </div>
                <! --/panel-body -->

            </div><!-- /col-lg-12 -->
        </div>
        <! --/row -->
    </div>
    <! --/container -->
</section>
@endsection
