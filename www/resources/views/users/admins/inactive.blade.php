@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div class="row content-panel">
                <div id="all" class="tab-pane active">
                    <div class="col-md-12 detailed">
                        <div class="table-responsive">
                            <h2>Inactieve gebruikers
                                            <span class="pull-right">
                                                <a href="/users/admins/create" class="btn btn-theme03">Nieuwe gebruiker</a>
                                            </span>
                            </h2>
                            <div class="search">



                            </div>

                            @if ($admins->count())
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            @if ($sortby == 'id' && $order == 'asc') {{ link_to_action(
                                                            'AdminsController@index',
                                                            '#',
                                                            array(
                                                            'sortby' => 'id',
                                                            'order' => 'desc'
                                                        )) }} <i class="fa fa-sort-numeric-asc"></i>
                                            @else {{ link_to_action(
                                                        'AdminsController@index',
                                                        '#',
                                                        array(
                                                            'sortby' => 'id',
                                                            'order' => 'asc'
                                                        )) }} <i class="fa fa-sort-numeric-desc"></i>
                                            @endif
                                        </th>
                                        <th>
                                            @if ($sortby == 'name' && $order == 'asc') {{ link_to_action(
                                                            'AdminsController@index',
                                                            'Naam',
                                                            array(
                                                            'sortby' => 'name',
                                                            'order' => 'desc'
                                                        )) }} <i class="fa fa-sort-alpha-asc"></i>
                                            @else {{ link_to_action(
                                                        'AdminsController@index',
                                                        'Naam',
                                                        array(
                                                            'sortby' => 'name',
                                                            'order' => 'asc'
                                                        )) }} <i class="fa fa-sort-alpha-desc"></i>
                                            @endif
                                        </th>
                                        <th>E-mail</th>
                                        <th>Status</th>
                                        <th>Aangemaakt</th>
                                        <th>Aangepast</th>
                                        <th>Acties</th>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    @foreach ($admins as $admin)
                                        <tr>
                                            <td>{{ $admin->id }}</td>
                                            <td>{{ $admin->name }}</td>
                                            <td>{{$admin->email}}</td>
                                            <td>@if($admin->active == 1 && $admin->disabled == 0)
                                                    <span class="label label-success">Active</span>
                                                @endif
                                                @if($admin->disabled == 1)
                                                    <span class="label label-danger">Banned</span>
                                                @endif
                                                @if($admin->active == 0 && $admin->disabled == 0)
                                                    <span class="label label-default">Inactive</span>
                                                @endif
                                                @if($admin->active == 1 && $admin->disabled == 1)
                                                    <span class="label label-default">Still active!</span>
                                                @endif</td>
                                            <td>{{$admin->created_at->format('m/d/Y')}}</td>
                                            <td>{{$admin->updated_at->format('m/d/Y')}}</td>
                                            <td style="width: 11.45%">
                                                <a href="../{{$admin->id}}" class="btn btn-default"
                                                   aria-expanded="false">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a class="btn btn-default"
                                                   href="/users/admins/{!! $admin->id !!}/edit"
                                                   aria-expanded="false">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <div aria-expanded="false" class="pull-right">
                                                    {!! Form::model($admin, ['method' => 'DELETE', 'action' => ['AdminsController@softDelete', $admin->id], 'onsubmit' => 'return AlertDelete()']) !!}
                                                    {!! Form::button('<i class="fa fa-ban"></i>', array(
                                                    'type' => 'submit',
                                                    'aria-expanded' => 'false',
                                                    'class'=> 'btn btn-default',
                                                    'onclick'=>'return confirm("Are you sure you want to delete this?")'
                                                    )) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="col-md-12 text-center">
                                    {!! $admins->links() !!}
                                </div>
                            @else
                                There are no inactive admins
                            @endif
                        </div>
                        {{--table-responsive--}}
                    </div>
                    {{--col-md-12 detailed--}}
                </div>
                {{--tab-panel--}}
            </div>
            {{--/row content-panel--}}
        </div>
        {{--/col-lg-12--}}
    </div>
    {{--/row--}}
</section>
@endsection
