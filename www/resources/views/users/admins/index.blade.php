@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div class="row content-panel">
                <div id="all" class="tab-pane active">
                    <div class="col-md-12 detailed">
                        <div class="table-responsive">
                            <h2>Alle gebruikers
                                <span class="pull-right">
                                    <a href="/users/admins/create" class="btn btn-theme03">Nieuwe gebruiker</a>
                                </span>
                            </h2>
                            <div class="row no-gutter">
                                <div class="form-group col-md-4 row no-gutter">
                                    <div class="col-md-10">
                                        <input type="text" placeholder="Zoek ..." class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <button onsubmit="" class="btn btn-theme03"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-md-1 centered">
                                    <a href="{{URL::route('users.admins.export')}}" class="btn btn-theme02"><i
                                                class="fa fa-download"></i></a>
                                    {{--<a href="{{URL::route('users.export')}}" class="btn btn-theme"><i class="fa fa-upload"></i></a>--}}
                                </div>
                                {{--<div class="form-group col-md-4 row no-gutter">
                                    {!! Form::model(['method' => 'POST', 'action' => 'AdminsController@postUploadCsv']) !!}
                                    {!! Form::file('file', ['class' => 'col-xs-10 pull-right']) !!}
                                    {!! Form::button('<i class="fa fa-upload"></i>', ['class' => 'btn btn-theme03 pull-left']) !!}
                                    {!! Form::close() !!}
                                </div>--}}


                                <div class="form-group col-md-3 row no-gutter">
                                    <form action="{{ URL::to('importExcelAdmins') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input class="col-xs-10 pull-left" type="file" name="import_file" />
                                        <button class="btn btn-theme04 pull-right"><i class="fa fa-upload"></i></button>
                                    </form>
                                </div>
                            </div>
                            {{--<table class="table">--}}
                            {{--<thead>--}}
                            {{--<tr>--}}
                            {{--<th>@sortablelink ('id')</th>--}}
                            {{--<th>@sortablelink ('name')</th>--}}
                            {{--<th>E-mail</th>--}}
                            {{--<th>Created</th>--}}
                            {{--<th>Updated</th>--}}
                            {{--<th>Actions</th>--}}
                            {{--</tr>--}}
                            {{--</thead>--}}
                            {{--<tbody>--}}

                            {{--@foreach ($admins as $admin)--}}
                            {{--<tr>--}}
                            {{--<td>{{$admin->id}}</td>--}}
                            {{--<td>{{$admin->name}}</td>--}}
                            {{--<td>{{$admin->email}}</td>--}}
                            {{--<td>{{$admin->created_at}}</td>--}}
                            {{--<td>{{$admin->updated_at}}</td>--}}
                            {{--<td><a href="users/{{$admin->id}}"><i class="fa fa-eye"></i></a></td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                            {{--</tbody>--}}
                            {{--</table>--}}
                            {{--<div class="col-md-12 text-center">--}}
                            {{--{!! $admins->appends(\Input::except('page'))->render() !!}--}}
                            {{--</div>--}}
                            @if ($admins->count())
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>
                                            @if ($sortby == 'id' && $order == 'asc') {{ link_to_action(
                                                            'AdminsController@index',
                                                            '#',
                                                            array(
                                                            'sortby' => 'id',
                                                            'order' => 'desc'
                                                        )) }} <i class="fa fa-sort-numeric-asc"></i>
                                            @else {{ link_to_action(
                                                        'AdminsController@index',
                                                        '#',
                                                        array(
                                                            'sortby' => 'id',
                                                            'order' => 'asc'
                                                        )) }} <i class="fa fa-sort-numeric-desc"></i>
                                            @endif
                                        </th>
                                        <th>
                                            @if ($sortby == 'name' && $order == 'asc') {{ link_to_action(
                                                            'AdminsController@index',
                                                            'Naam',
                                                            array(
                                                            'sortby' => 'name',
                                                            'order' => 'desc'
                                                        )) }} <i class="fa fa-sort-alpha-asc"></i>
                                            @else {{ link_to_action(
                                                        'AdminsController@index',
                                                        'Naam',
                                                        array(
                                                            'sortby' => 'name',
                                                            'order' => 'asc'
                                                        )) }} <i class="fa fa-sort-alpha-desc"></i>
                                            @endif
                                        </th>
                                        <th>Voornaam</th>
                                        <th>Achternaam</th>
                                        <th>Studentennummer</th>
                                        <th>Studentengroep</th>
                                        <th>E-mail</th>
                                        <th>Status</th>
                                        <th>Aangemaakt</th>
                                        <th>Aangepast</th>
                                        <th>Acties</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($admins as $admin)
                                        <tr>
                                            <td>{{ $admin->id }}</td>
                                            <td>{{ $admin->name }}</td>
                                            <td>{{ $admin->first_name }}</td>
                                            <td>{{ $admin->last_name }}</td>
                                            <td>{{ $admin->student_number }}</td>
                                            <td>{{ $admin->group }}</td>
                                            <td>{{$admin->email}}</td>
                                            <td>@if($admin->active == 1 && $admin->disabled == 0)
                                                    <span class="label label-success">Active</span>
                                                @endif
                                                @if($admin->disabled == 1)
                                                    <span class="label label-danger">Banned</span>
                                                @endif
                                                @if($admin->active == 0 && $admin->disabled == 0)
                                                    <span class="label label-default">Inactive</span>
                                                @endif
                                                @if($admin->active == 1 && $admin->disabled == 1)
                                                    <span class="label label-default">Still active!</span>
                                                @endif</td>
                                            <td>{{$admin->created_at->format('m/d/Y')}}</td>
                                            <td>{{$admin->updated_at->format('m/d/Y')}}</td>
                                            <td style="width: 11.45%">
                                                <a href="users/admins/{{$admin->id}}" class="btn btn-default"
                                                   aria-expanded="false">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                <a class="btn btn-default"
                                                   href="/users/admins/{!! $admin->id !!}/edit"
                                                   aria-expanded="false">
                                                    <i class="fa fa-pencil"></i>
                                                </a>
                                                <div aria-expanded="false" class="pull-right">
                                                    {!! Form::model($admin, ['method' => 'DELETE', 'action' => ['AdminsController@softDelete', $admin->id], 'onsubmit' => 'return AlertDelete()']) !!}
                                                    {!! Form::button('<i class="fa fa-ban"></i>', array(
                                                    'type' => 'submit',
                                                    'aria-expanded' => 'false',
                                                    'class'=> 'btn btn-default',
                                                    'onclick'=>'return confirm("Are you sure you want to delete this?")'
                                                    )) !!}
                                                    {!! Form::close() !!}
                                                </div>
                                                {{--<div aria-expanded="false" class="pull-right">--}}
                                                {{--{!! Form::model($admin, ['method' => 'DELETE', 'action' => ['AdminsController@destroyAdmin', $admin->id], 'onsubmit' => 'return AlertDelete()']) !!}--}}
                                                {{--{!! Form::button('<i class="fa fa-times"></i>', array(--}}
                                                {{--'type' => 'submit',--}}
                                                {{--'aria-expanded' => 'false',--}}
                                                {{--'class'=> 'btn btn-default',--}}
                                                {{--'onclick'=>'return confirm("Are you sure you want to delete this?")'--}}
                                                {{--)) !!}--}}
                                                {{--{!! Form::close() !!}--}}
                                                {{--</div>--}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="col-md-12 text-center no-margin">
                                    {!! $admins->links() !!}
                                </div>
                            @else
                                There are no admins
                            @endif
                        </div>
                        {{--table-responsive--}}
                    </div>
                    {{--col-md-12 detailed--}}
                </div>
                {{--tab-panel--}}
            </div>
            {{--/row content-panel--}}
        </div>
        {{--/col-lg-12--}}
    </div>
    {{--/row--}}
</section>
@endsection
