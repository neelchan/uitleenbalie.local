@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div class="row content-panel">
                <div id="all" class="tab-pane active">
                    <div class="col-md-12 detailed">
                        <div class="table-responsive">
                                    <h2>Verwijderde gebruikers
                                            <span class="pull-right">
                                                <a href="/users/admins/create" class="btn btn-theme03">Nieuwe gebruiker</a>
                                            </span>
                                    </h2>

                                    @if ($admins->count())
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>
                                                    @if ($sortby == 'id' && $order == 'asc') {{ link_to_action(
                                                            'AdminsController@index',
                                                            '#',
                                                            array(
                                                            'sortby' => 'id',
                                                            'order' => 'desc'
                                                        )) }} <i class="fa fa-sort-numeric-asc"></i>
                                                    @else {{ link_to_action(
                                                        'AdminsController@index',
                                                        '#',
                                                        array(
                                                            'sortby' => 'id',
                                                            'order' => 'asc'
                                                        )) }} <i class="fa fa-sort-numeric-desc"></i>
                                                    @endif
                                                </th>
                                                <th>
                                                    @if ($sortby == 'name' && $order == 'asc') {{ link_to_action(
                                                            'AdminsController@index',
                                                            'Naam',
                                                            array(
                                                            'sortby' => 'name',
                                                            'order' => 'desc'
                                                        )) }} <i class="fa fa-sort-alpha-asc"></i>
                                                    @else {{ link_to_action(
                                                        'AdminsController@index',
                                                        'Naam',
                                                        array(
                                                            'sortby' => 'name',
                                                            'order' => 'asc'
                                                        )) }} <i class="fa fa-sort-alpha-desc"></i>
                                                    @endif
                                                </th>
                                                <th>E-mail</th>
                                                <th>Status</th>
                                                <th>Aangemaakt</th>
                                                <th>Aangepast</th>
                                                <th>Verwijderd</th>
                                                <th>Acties</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach ($admins as $admin)
                                                <tr>
                                                    <td>{{ $admin->id }}</td>
                                                    <td>{{ $admin->name }}</td>
                                                    <td>{{$admin->email}}</td>
                                                    <td>@if($admin->active == 1 && $admin->disabled == 0)
                                                            <span class="label label-success">Active</span>
                                                        @endif
                                                        @if($admin->disabled == 1)
                                                            <span class="label label-danger">Banned</span>
                                                        @endif
                                                        @if($admin->active == 0 && $admin->disabled == 0)
                                                            <span class="label label-default">Inactive</span>
                                                        @endif
                                                        @if($admin->active == 1 && $admin->disabled == 1)
                                                            <span class="label label-default">Still active!</span>
                                                        @endif</td>
                                                    <td>{{$admin->created_at->format('m/d/Y')}}</td>
                                                    <td>{{$admin->updated_at->format('m/d/Y')}}</td>
                                                    <td>{{$admin->deleted_at->format('m/d/Y')}}</td>
                                                    <td>
                                                        <div aria-expanded="false" class="centered">
                                                            {!! Form::model($admin, ['method' => 'PATCH', 'action' => ['AdminsController@restore', $admin->id], 'onsubmit' => 'return AlertDelete()']) !!}
                                                            {!! Form::button('<i class="fa fa-unlock-alt"></i>', array(
                                                            'type' => 'submit',
                                                            'aria-expanded' => 'false',
                                                            'class'=> 'btn btn-default',
                                                            'onclick'=>'return confirm("Are you sure you want to restore this?")'
                                                            )) !!}
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="col-md-12 text-center">
                                            {!! $admins->links() !!}
                                        </div>
                                    @else
                                        There are no deleted admins
                                    @endif
                        </div>
                        {{--table-responsive--}}
                    </div>
                    {{--col-md-12 detailed--}}
                </div>
                {{--tab-panel--}}
            </div>
            {{--/row content-panel--}}
        </div>
        {{--/col-lg-12--}}
    </div>
    {{--/row--}}
</section>
@endsection
