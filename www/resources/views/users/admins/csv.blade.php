<table class="table">
    <thead>
    <tr>
        <td>ID</td>
        <td>name</td>
        <td>first_name</td>
        <td>last_name</td>
        <td>student_number</td>
        <td>group</td>
        <td>email</td>
        <td>active</td>
        <td>disabled</td>
        <td>created_at</td>
        <td>updated_at</td>
        <td>deleted_at</td>
    </tr>
    </thead>
    <tbody>
    @foreach ($admins as $admin)
        <tr>
            <td>{{$admin['id']}}</td>
            <td>{{$admin['name']}}</td>
            <td>{{$admin['first_name']}}</td>
            <td>{{$admin['last_name']}}</td>
            <td>{{$admin['student_number']}}</td>
            <td>{{$admin['group']}}</td>
            <td>{{$admin['email']}}</td>
            <td>{{$admin['active']}}</td>
            <td>{{$admin['disabled']}}</td>
            <td>{{$admin['created_at']}}</td>
            <td>{{$admin['updated_at']}}</td>
            <td>{{$admin['deleted_at']}}</td>

        </tr>
    @endforeach
    </tbody>
</table>

