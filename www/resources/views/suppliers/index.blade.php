@extends('layouts.app')

@section('content')
<section class="wrapper site-min-height full-height">
    <div class="row">
        <div class="col-lg-12">
            <div ng-controller="SuppliersCtrl">
                <div class="row content-panel">
                    <div class="col-md-12 detailed">
                        <div class="table-responsive">
                            <h2>Alle leveranciers
                                {{--<span class="pull-right">--}}
                                {{--<a href="/products/create" class="btn btn-theme03">Nieuw product</a>--}}
                                {{--</span>--}}
                            </h2>
                            <div class="row col-lg-12">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>@sortablelink ('id', '#')</th>
                                        <th>@sortablelink ('name', '#')</th>
                                        <th>Tel</th>
                                        <th>E-mail</th>
                                        <th>Meer</th>
                                        <th>Aangemaakt</th>
                                        <th>Aangepast</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
{{--
                                    <input type="text" ng-model="search">
--}}
                                        {{--<input type="text" class="form-control" ng-model="selectedSupplier" typeahead="supplier as supplier.registration for supplier in getSupplier($viewValue) | filter:$viewValue | limitTo:3" placeholder="Search for an supplier" typeahead-template-url="templates/supplier-tpl.html">--}}
                                        {{--<div class="row">--}}
                                            {{--<div class="col-lg-6">--}}
                                                {{--<h1>Selected Supplier</h1>--}}
                                                {{--<pre>@{{selectedSupplier | json}}</pre>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--<ul ng-repeat="supplier in SuppliersCtrl($viewValue)">
                                        <li>@{{supplier.id}}</li>
                                    </ul>--}}
                                    @foreach ($suppliers as $supplier)
                                        <tr>
                                            <td>{{$supplier->id}}</td>
                                            <td>{{$supplier->company_name}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        @endforeach
                                    </tbody>

                                </table>
                                <div class="col-md-12 text-center">
                                    {{--{!! $products->appends(\Input::except('page'))->render() !!}--}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div ng-controller="AirplanesCtrl">
            <div class="col-lg-6">
                <h1>Airplane Search</h1>
                <!--            <input type="text" class="form-control" placeholder="Search for an airplane" ng-model="selectedAirplane" uib-typeahead="airplane as airplane.registration for airplane in airplanes | filter:$viewValue | limitTo:3" typeahead-template-url="templates/airplane-tpl.html">-->
                <input type="text" class="form-control" placeholder="Search for an airplane" ng-model="selectedAirplane"
                       uib-typeahead="airplane as airplane.operator for airplane in airplanes | filter:$viewValue | limitTo:3"
                       uib-typeahead-template-url="templates/airplane-tpl.html">


                <div class="search">
                    <h1>Simple search</h1>
                    <input type="text" ng-model="search">
                    <div class="row col-lg-12">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>registration</th>
                                <th>operator</th>
                                <th>manufacturer</th>
                                <th>type</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{--<tr>@{{airplaine.registration}}</tr>--}}
                            {{--<tr>@{{airplaine.operator}}</tr>--}}
                            {{--<tr>@{{airplaine.manufacturer}}</tr>--}}
                            {{--<tr>@{{airplaine.type}}</tr>--}}
                            <tr ng-repeat="airplaine in airplanes | filter:search">
                                <td>@{{ $index + 1 }}</td>
                                <td>@{{airplaine.registration}}</td>
                                <td>@{{airplaine.operator}}</td>
                                <td>@{{airplaine.manufacturer}}</td>
                                <td>@{{airplaine.type}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <h1>Selected Airplane</h1>
                    <pre>@{{selectedAirplane | json}}</pre>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
