@extends('layouts.frontoffice')

@section('content')
    <section class="bg-primary" id="home">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">We've got what you need!</h2>
                    <hr class="light">
                    <p class="text-faded">Heb je het volledige materiaal nodig om een
                        fotostudio te maken? <br> Of ben je op zoek naar een camera? Of heb je ICT-tools nodig? <br> Dan
                        ben
                        je hier bij het goede adres!</p>
                    <!--<a href="catalogus.html" class="btn btn-default btn-xl">Zoek jouw materiaal!</a>-->
                    {{--<div class="container-search">--}}
                        {{--<span class="icon"><i class="fa fa-search"></i></span>--}}
                        {{--<input type="search" id="search" placeholder="Zoek jouw materiaal!"/>--}}
                    {{--</div>--}}

                    {!! Form::open(['route' => 'catalogus.search', 'method' => 'GET', 'class' => '', 'role' => 'search']) !!}
                    <div class="container-search">
                        <span class="icon"><i class="fa fa-search" style="color: #aaa;"></i></span>
                        {!! Form::text('term', Request::get('term'), ['class' => '', 'id' => 'search', 'placeholder' => 'Zoek jouw materiaal!']) !!}
                    </div>
                    {!! Form::close() !!}
                    <div class="row">
                        <div class="col-lg-12 tags">
                            @foreach ($products_brand as $name => $brand)
                                <a href="/catalogus?term={{ $brand }}"><span class="label label-default">{{ $brand }}</span></a>
                            @endforeach

                            @foreach ($products_type as $name => $type)
                                    <a href="/catalogus?term={{ $type }}"><span class="label label-default">{{ $type }}</span></a>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="portfolio">
        <div class="col-lg-12 text-center">
            <h2 class="section-heading">Meest gehuurde producten</h2>
            <hr class="orange">
        </div>

        <div class="container-fluid">
            <div class="row no-gutter">
                @foreach($mostRentedProducts as $index => $product)
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 product product-home">
                        <div class="content">
                            <a href="catalogus/{{$product->id}}" class="portfolio-box">
                                <div class="product-image">
                                    @if($product->active == 1)
                                        <span class="top green"><i class="fa fa-calendar-check-o"></i> Beschikbaar</span>
                                    @else
                                        <span class="top pink"><i class="fa fa-clock-o"></i> Niet beschikbaar</span>
                                    @endif
                                    <img src="/uploads/products/{{ $product->image }}"
                                         class="img-responsive" alt="">
                                </div>
                                <div class="portfolio-box-caption">
                                    <div class="portfolio-box-caption-content">
                                        <div class="product-name">
                                            RESERVEER
                                        </div>
                                    </div>
                                </div>
                            </a>
                            <div class="description">
                                <h3>{{ $product->name }}</h3>
                                <!--<span class="right"><a href="#" data-toggle="tooltip" data-placement="right" title="Niet beschikbaar"><i class="fa fa-clock-o faa-parent animated-hover pink"></i></a></span>-->
                                <p>
                                    {{--{{ $product->description }}--}}
                                    short description
                                </p>
                                <p><i class="fa fa-tag"></i> {{ $product->tags }} </p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

    </section>

    <section class="no-padding" id="interesting">
        <aside class="bg-pink">
            <div class="col-lg-12 text-center">
                <h2 class="section-heading">Nieuwe producten</h2>
                <hr class="light">
            </div>
            <div class="container-fluid">
                <div class="row no-gutter">
                    @foreach($latestProducts as $index => $product)
                        <div class="col-lg-4 col-sm-6 product product-color">
                            <div class="content">
                                <a href="catalogus/{{$product->id}}" class="portfolio-box">
                                    <div class="product-image">
                                        @if($product->active == 1)
                                            <span class="top green"><i class="fa fa-calendar-check-o"></i> Beschikbaar</span>
                                        @else
                                            <span class="top pink"><i class="fa fa-clock-o"></i> Niet beschikbaar</span>
                                        @endif
                                        <img src="/uploads/products/{{ $product->image }}"
                                             class="img-responsive" alt="">
                                    </div>
                                    <div class="portfolio-box-caption">
                                        <div class="portfolio-box-caption-content">
                                            <div class="product-name">
                                                RESERVEER
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="description">
                                    <h3>{{ $product->name }}</h3>
                                    <!--<span class="right"><a href="#" data-toggle="tooltip" data-placement="right" title="Niet beschikbaar"><i class="fa fa-clock-o faa-parent animated-hover pink"></i></a></span>-->
                                    <p>
                                        {{--{{ $product->description }}--}}
                                        short description
                                    </p>
                                    <p><i class="fa fa-tag"></i> {{ $product->tags }} </p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </aside>
    </section>
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">At Your Service</h2>
                    <hr class="blue">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-paper-plane wow bounceIn faa-float animated text-primary"
                           data-wow-delay=".1s"></i>

                        <h3>Snelle service</h3>

                        <p class="text-muted">Kijk of er iemand aanwezig is in onze C-blok. Daar kunnen ze u helpen om
                            het
                            geschikte materiaal te huren.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-newspaper-o wow bounceIn faa-float animated text-primary"
                           data-wow-delay=".2s"></i>

                        <h3>Up to Date</h3>

                        <p class="text-muted">Al het materiaal is te vinden in onze catalogus. Je kunt ook zien of deze
                            al
                            verhuurd is of niet. Wij houden u ook op de hoogte van alle nieuwe materialen.</p>
                    </div>
                </div>
                <div class="col-lg-4 col-sm-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-heart wow bounceIn faa-float animated text-primary"
                           data-wow-delay=".3s"></i>

                        <h3>Favorite</h3>

                        <p class="text-muted">Je ziet jouw favoriete producten op je profiel, deze kan je met enkele
                            klikjes
                            opnieuw huren.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
