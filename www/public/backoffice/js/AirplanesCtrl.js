app.controller('AirplanesCtrl', function ($scope, $http) {
    $http.get('data/airplanes.json').success(function (data) {
        $scope.airplanes = data;
    });
});
app.controller('SuppliersCtrl', function ($scope, $http) {

    $scope.getSupplier = function() {
        return $http.get('api/suppliers/').then(function(data) {
            return data.data;
        });
    };
});


//app.controller('SuppliersCtrl', function($scope, $http, API_URL) {
//    //retrieve suppliers listing from API
//    $http.get(API_URL + "suppliers")
//        .success(function (response) {
//            $scope.suppliers = response;
//        });
//
//})