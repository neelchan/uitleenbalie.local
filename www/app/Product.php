<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;
//use Carbon;

class Product extends Model
{
    use Sortable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'product_code', 'brand', 'brand_number', 'serial_number', 'production_date',
        'image', 'description', 'tags', 'QR_code', 'purchase_price', 'purchase_date',
        'CanBeLentByTeacher', 'CanBeLentByStudent', 'type', 'active', 'deleted_at', 'created_at', 'updated_at'
    ];

    protected $sortable = ['id',
        'name', 'product_code', 'brand', 'brand_number', 'serial_number', 'production_date',
        'image', 'description', 'tags', 'QR_code', 'purchase_price', 'purchase_date',
        'CanBeLentByTeacher', 'CanBeLentByStudent', 'type', 'active', 'created_at', 'updated_at'];

    protected $dates = ['deleted_at', 'created_at', 'updated_at', 'production_date', 'purchase_date'];

    public function setCreatedAtAttribute($date)
    {
        $this->attributes['created_at'] = Carbon::parse($date);
    }

    public function setUpdatedAtAttribute($date)
    {
        $this->attributes['updated_at'] = Carbon::parse($date);
    }
}
