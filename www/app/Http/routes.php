<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {


    Route::get('products/export', array('uses' => 'ProductController@export', 'as' => 'products.export'));
    Route::get('users/export', array('uses' => 'UsersController@export', 'as' => 'users.export'));



    //
});

Route::group(
    [
        'as' => '',
        'prefix' => '',
        'middleware' => 'web',
    ],
    function () {

        Route::auth();

        Route::get('/home', 'FrontofficeController@index');
        Route::get('/', 'FrontofficeController@index');

        Route::get('/catalogus', 'FrontofficeController@catalogus');

        Route::get('/catalogus', array('uses' => 'FrontofficeController@search', 'as' => 'catalogus.search'));

        Route::get('/account', 'FrontofficeController@account');
        Route::get('/account/{id}/edit', 'FrontofficeController@editUser');
        Route::post('/account/{id}', 'FrontofficeController@updateUser');
        Route::get('/account/{id}/editImage', 'FrontofficeController@editUserImage');
        Route::put('/account/{id}', 'FrontofficeController@updateUserImage');


        Route::get('/account/{id}', 'FrontofficeController@account');

        Route::get('/contact', 'FrontofficeController@contact');
        Route::get('/catalogus/{id}', 'FrontofficeController@show');

    }
);


Route::group(['prefix' => 'api'], function()
{
    Route::resource('suppliers', 'SuppliersController', array('only' => 'show'));

});

Route::group(
    [
        'middleware' => 'web',
        'as' => 'backoffice',
        'prefix' => 'backoffice',
    ],

    function () {
        //Route::get('/products', array('uses' => 'ProductController@search', 'as' => 'backoffice.products.search'));


        Route::auth();

        //Route::get('/backoffice/home', 'HomeController@index');
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index');



    Route::get('suppliers', 'SuppliersController@index');
    Route::get('calendar', 'HomeController@calendar');
/*    Route::get('/suppliers', function()
    {
        return View::make('suppliers.index');
    });*/

    //Admins
    Route::get('/users/admins', 'AdminsController@index');

    Route::post('importExcelAdmins', 'AdminsController@importExcel');

    Route::get('/users/admins/inactive', 'AdminsController@Inactive');
    Route::get('/users/admins/deleted', 'AdminsController@showSoftDeleteResults');
    Route::get('/users/admins/create', 'AdminsController@newAdmin');
    Route::get('/users/admins/{id}/edit', 'AdminsController@editAdmin');
    Route::post('/users/admins/{id}', 'AdminsController@updateAdmin');
    Route::get('/users/admins/{id}', 'AdminsController@show');
    Route::post('/users/admins/', 'AdminsController@create');
    Route::patch('/users/admins/{id}', 'AdminsController@softDelete');
    Route::patch('/users/admins/{id}', 'AdminsController@restore');
    Route::delete('/users/admins/{id}', 'AdminsController@destroyAdmin');

    //Users
    Route::get('/users', 'UsersController@index');
    Route::get('/users', 'UsersController@search');


    Route::post('importExcelUsers', 'UsersController@importExcel');
    Route::get('/users/inactive', 'UsersController@Inactive');
    Route::get('/users/deleted', 'UsersController@showSoftDeleteResults');
    Route::get('/users/create', 'UsersController@newUser');
    Route::get('/users/{id}/edit', 'UsersController@editUser');
    Route::get('/users/{id}/editImage', 'UsersController@editUserImage');
    Route::post('/users/{id}', 'UsersController@updateUser');
    Route::put('/users/{id}', 'UsersController@updateUserImage');
    Route::get('/users/{id}', 'UsersController@show');
    Route::post('/users', 'UsersController@create');
    Route::patch('/users/{id}', 'UsersController@softDelete');
    Route::patch('/users/{id}', 'UsersController@restore');
    Route::delete('/users/{id}', 'UsersController@destroyUser');


    //Products
    Route::get('/products', 'ProductController@index');
    Route::get('/products', 'ProductController@search');


    Route::post('importExcelProducts', 'ProductController@importExcel');



    Route::get('/products/create', 'ProductController@newProduct');
    Route::get('/products/{id}/edit', 'ProductController@editProduct');
    Route::get('/products/{id}/editImage', 'ProductController@editProductImage');
    Route::get('/products/{id}', 'ProductController@show');
    Route::get('/products/subpages/deleted', 'ProductController@showSoftDeleteResults');
    Route::get('/products/subpages/inactive', 'ProductController@Inactive');
    Route::post('/products/{id}', 'ProductController@updateProduct');
    Route::put('/products/{id}', 'ProductController@updateProductImage');
    Route::post('/products', 'ProductController@create');
    Route::patch('/products/{id}', 'ProductController@softDelete');
    Route::patch('/products/{id}', 'ProductController@restore');
    Route::delete('/products/{id}', 'ProductController@destroyProduct');


});


