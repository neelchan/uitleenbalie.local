<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ShareRequest;
use App\Supplier;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Input;
use Redirect;
use Session;
use Validator;


//use Symfony\Component\HttpFoundation\JsonResponse;


class SuppliersController extends Controller
{
/*    public function index(Supplier $supplier)
    {

        $suppliers = $supplier->latest()->paginate(10);

        return view('suppliers.index', compact('suppliers'));

    }*/
    public function index(Supplier $supplier)
    {
        $suppliers = $supplier->sortable()->paginate(10);

        return view('suppliers.index', compact('suppliers'));
    }
//    public function index()
//    {
////        $suppliers = $supplier->get();
//
//        return view('suppliers.index');
//
//    }

    public function show()
    {

        $suppliers = Supplier::all();

        foreach ($suppliers as $supplier) {

            $response[] = [
                'id' => $supplier->id,
                'company_name' => $supplier->company_name,
                'telephone_number' => $supplier->telephone_number,
                'global_email' => $supplier->global_email,
                'description' => $supplier->description,
                'created_at' => $supplier->created_at,
                'updated_at' => $supplier->updated_at,
                'deleted_at' => $supplier->deleted_at
            ];
        }

        return response()->json($response, 200);


    }

//    public function show($term)
//    {
//        $suppliers = DB::table('suppliers')
//            ->where('company_name', 'LIKE', '%' . $term . '%')
//            ->orWhere('global_email', 'LIKE', '%' . $term . '%')
//            ->orWhere('description', 'LIKE', '%' . $term . '%')
//            ->get();
//
//        return Response::json($suppliers, 200);
//    }

//
//        return view('suppliers.index');
//
//
//        Request $request
//        $input = $request->all();
//        if($request->get('search')){
//            $items = Supplier::where("company_name", "LIKE", "%{$request->get('search')}%")
//                ->paginate(5);
//        }else{
//            $items = Supplier::paginate(5);
//        }
//        return response($items);
//    }
//
//    public function index($id = null) {
//        if ($id == null) {
//            return Supplier::orderBy('id', 'asc')->get();
//        } else {
//            return $this->show($id);
//        }
//
//
//    }
}
