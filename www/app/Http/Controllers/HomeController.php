<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $userStats = $this->getUsersStats();
        $productStats = $this->getProductsStats();
        $latestProducts = $this->getLatestProducts();

        return view('home')->with(['userStats' => $userStats, 'productStats' => $productStats, 'latestProducts' => $latestProducts]);

//        return view('home');
    }

    public function calendar()
    {
        return view('calendar');

    }

    public function getUsersStats()
    {
        $inactiveUsers =  DB::table('users')->where('active', '0')->count();
        $activeUsers =  DB::table('users')->where('active', '1')->count();
        $bannedUsers =  DB::table('users')->where('disabled', '1')->count();
        $bannedButActiveUsers =  DB::table('users')->where('disabled', '1')->where('active', '1')->count();
        $totalUsers = DB::table('users')->count();
        $newestUser = DB::table('users')->orderBy('created_at', 'desc')->first();

        return [
            'active' => $activeUsers,
            'inactive' => $inactiveUsers,
            'banned' => $bannedUsers,
            'bannedButActive' => $bannedButActiveUsers,
            'total' => $totalUsers,
            'newest' => $newestUser->name,
            'newestEmail' => $newestUser->email,
            'newestDate' => $newestUser->created_at
        ];
    }


    public function getProductsStats()
    {
//        $inactiveProducts =  DB::table('products')->where('active', '0')->count();
//        $activeProducts =  DB::table('products')->where('active', '1')->count();
//        $bannedProducts =  DB::table('products')->where('disabled', '1')->count();
        $totalProducts = DB::table('products')->count();
        $newestProduct = DB::table('products')->orderBy('created_at', 'desc')->first();


        return [
//            'active' => $inactiveProducts,
//            'inactive' => $activeProducts,
//            'banned' => $bannedProducts,
            'total' => $totalProducts,
            'newest' => $newestProduct->name,
            'newestFile' => $newestProduct->image];
    }

    public function getLatestProducts(){


        $results = DB::table('products')->where('deleted_at', null)->orderBy('created_at', 'desc')->get();

        $collection = new Collection($results);

        $top5 = $collection->take(3);

        return $top5;
    }
}
