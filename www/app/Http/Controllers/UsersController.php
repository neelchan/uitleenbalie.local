<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Input;
use Kyslik\ColumnSortable;
use Redirect;
use Session;
use Validator;
use Excel;
//use Illuminate\Foundation\Auth\User;
//use Illuminate\Support\Facades\Input;


use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UsersController extends Controller
{

    public function index(User $user)
    {
        $sortby = Input::get('sortby');
        $order = Input::get('order');

        if ($sortby && $order) {
            $users = $user->orderBy($sortby, $order)->paginate(10);
        } else {
            $users = $user->paginate(10);
        }

        return view('users.index', compact('users', 'sortby', 'order'));


    }
    public function search(Request $request, User $user)
    {
        $sortby = Input::get('sortby');
        $order = Input::get('order');

        $users =  $user->sortable()->where(function($query) use ($request) {

            if ( ($term = $request->get('term')) ) {
                $query->orWhere('name', 'like', '%' . $term . '%');
                $query->orWhere('first_name', 'like', '%' . $term . '%');
                $query->orWhere('last_name', 'like', '%' . $term . '%');
                $query->orWhere('student_number', 'like', '%' . $term . '%');
                $query->orWhere('group', 'like', '%' . $term . '%');
            }
        })
            ->paginate(10);

        return view('users.index', compact('users', 'sortby', 'order'));


    }

    public function Inactive(User $user)
    {

        $sortby = Input::get('sortby');
        $order = Input::get('order');

        if ($sortby && $order) {
            $users = $user->where('disabled', '1')->orderBy($sortby, $order)->paginate(10);
        } else {
            $users = $user->where('disabled', '1')->paginate(10);
        }

        return view('users.inactive', compact('users', 'sortby', 'order'));


    }

    public function showSoftDeleteResults(User $user)
    {

        $sortby = Input::get('sortby');
        $order = Input::get('order');

        if ($sortby && $order) {
            $users = $user->onlyTrashed()->orderBy($sortby, $order)->paginate(10);
        } else {
            $users = $user->onlyTrashed()->paginate(10);
        }

        return view('users.deleted', compact('users', 'sortby', 'order'));
    }

    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('users.show', compact('user'));
    }

    public function newUser()
    {
        return view('users.create');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    public function create(Request $input)
    {
        $image = $input->file('image');
        $upload = 'uploads/users';
        $filename = $image->getClientOriginalName();
        $success = $image->move($upload, $filename);

        if($success){
            $table = new User();
            $table->name = $input->Input('name');
            $table->email = $input->Input('email');
            $table->first_name = $input->Input('first_name');
            $table->last_name = $input->Input('last_name');
            $table->student_number = $input->Input('student_number');
            $table->group = $input->Input('group');
            $table->active = $input->Input('active');
            $table->disabled = $input->Input('disabled');
            $table->password = bcrypt($input->Input('password'));
            $table->image = $filename;

            $table->save();
            return redirect('backoffice/users');

        }
        else{
            dd("user niet aangemaakt");
        }
        return redirect('backoffice/users');
    }

    public function destroyUser($id, Request $request) //delete the user
    {
        $user = User::find($id);

        $user->delete($request->all());

        return redirect('backoffice/users');

    }

    public function editUser($id) //view edit user page
    {
        $user = User::find($id);
        return view('users.edit', compact('user'));
    }
    public function editUserImage($id) //view edit product page
    {
        $user = User::find($id);
        return view('users.editImage', compact('user'));
    }

    public function updateUser($id, Request $request) //update user
    {
        $user = User::find($id);
        $user->update($request->all());
        return view('users.show', compact('user'));
    }
    public function updateUserImage($id, Request $input) //update user image
    {

        $user = User::find($id);
        $image = $input->file('image');
        $upload = 'uploads/users';
        $filename = $image->getClientOriginalName();
        $success = $image->move($upload, $filename);

        if($success){
            $user->image = $filename;
            $user->update();
            $user->save();
            return view('users.show', compact('user'));

        }
        else{
            dd("user niet aangemaakt");
        }
    }

    public function softDelete($id, Request $request)
    {

        $user = User::find($id);

        $user->softDeletes($request->all());

        return redirect('users');
    }

    public function restore($id)
    {

        User::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect('backoffice/users');
    }
    public function export(){
        Excel::create('Users', function($excel) {

            $excel->sheet('Users', function($sheet) {
                $users = User::orderBy('created_at','desc')->get();
                $sheet->loadView('users.csv', ['users' => $users->toArray()]);
            });
        })->download('xls');
        //!!!! verwijderde gebruikers worden niet geexporteerd
    }

//    public function postUploadCsv()
//    {
//        $rules = array(
//            'file' => 'required',
//            'num_records' => 'required',
//        );
//
//        $validator = Validator::make(Input::all(), $rules);
//        // process the form
//        if ($validator->fails())
//        {
//            return Redirect::to('customer-upload')->withErrors($validator);
//        }
//        else
//        {
//            try {
//                Excel::load(Input::file('users'), function ($reader) {
//
//                    foreach ($reader->toArray() as $row) {
//                        User::firstOrCreate($row);
//                    }
//                });
//                \Session::flash('success', 'Users uploaded successfully.');
//                return redirect(route('users.index'));
//            } catch (\Exception $e) {
//                \Session::flash('error', $e->getMessage());
//                return redirect(route('users.index'));
//            }
//        }
//    }

    public function importExcel()
    {
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    User::create([
                        'name' => $value->name,
                        'first_name' => $value->first_name,
                        'last_name' => $value->last_name,
                        'student_number' => $value->student_number,
                        'group' => $value->group,
                        'email' => $value->email,
                        'active' => $value->active,
                        'disabled' => $value->disabled,
                    ]);
                }
            }
        }
        return redirect('backoffice/users');
    }


}
