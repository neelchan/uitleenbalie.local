<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

use Input;
use Validator;
use Redirect;
use Session;
use Excel;
use Illuminate\Support\Facades\DB;
use Kyslik\ColumnSortable;
use Carbon\Carbon;

class ProductController extends Controller
{
//    public function index()
//    {
//
//        $products = Product::latest()->paginate(10);
//
//        return view('products.index', compact('products'));
//    }
    public function index(Product $product)
    {
        $products = $product->sortable()->paginate(10);

        return view('products.index', compact('products'));
    }
    public function search(Request $request, Product $product)
    {
        $products =  $product->sortable()->where(function($query) use ($request) {

            if ( ($term = $request->get('term')) ) {
                $query->orWhere('name', 'like', '%' . $term . '%');
                $query->orWhere('brand', 'like', '%' . $term . '%');
                $query->orWhere('product_code', 'like', '%' . $term . '%');
                $query->orWhere('type', 'like', '%' . $term . '%');
            }
        })
            ->paginate(10);

        return view('products.index', compact('products'));


    }
    public function Inactive(Product $product)
    {

        $products = $product->sortable()->where('active', '0')->paginate(10);

        return view('products.subpages.inactive', compact('products'));

    }

    public function showSoftDeleteResults(Product $product)
    {

        $products = $product->onlyTrashed()->sortable()->paginate(10);

        return view('products.subpages.deleted', compact('products'));

    }

    public function show($id)
    {
        $product = Product::findOrFail($id);

        return view('products.show', compact('product'));
    }
    public function export(){
        Excel::create('products', function($excel) {

            $excel->sheet('products', function($sheet) {
                $products = Product::orderBy('id','desc')->get();
                $sheet->loadView('products.csv', ['products' => $products->toArray()]);
            });
        })->download('xls');
        // !! DELETED PRODUCTS ARE NOT INCLUDED !!
    }
    public function newProduct()
    {
        return view('products.create');
    }

    public function create(Request $input)
    {
        $image = $input->file('image');
        $upload = 'uploads/products';
        $filename = $image->getClientOriginalName();
        $success = $image->move($upload, $filename);

        if($success){
            $table = new Product();
            $table->product_code = $input->Input('product_code');
            $table->name = $input->Input('name');
            $table->brand = $input->Input('brand');
            $table->brand_number = $input->Input('brand_number');
            $table->serial_number = $input->Input('serial_number');
            $table->production_date = $input->Input('production_date');
            $table->description = $input->Input('description');
            $table->tags = $input->Input('tags');
            $table->QR_code = $input->Input('QR_code');
            $table->purchase_price = $input->Input('purchase_price');
            $table->purchase_date = $input->Input('purchase_date');
            $table->CanBeLentByTeacher = $input->Input('CanBeLentByTeacher');
            $table->CanBeLentByStudent = $input->Input('CanBeLentByStudent');
            $table->type = $input->Input('type');
            $table->active = $input->Input('active');
            $table->image = $filename;

            $table->save();
            return redirect('backoffice/products');
        }
        else{
            dd("product niet aangemaakt");
        }
        return redirect('backoffice/products');

    }
    public function destroyProduct($id, Request $request) //delete the product
    {
        $product = Product::find($id);

        $product->delete($request->all());

        return redirect('backoffice/products');

    }
    public function editProduct($id) //view edit product page
    {
        $product = Product::find($id);
        return view('products.edit', compact('product'));
    }
    public function editProductImage($id) //view edit product page
    {
        $product = Product::find($id);
        return view('products.editImage', compact('product'));
    }

    public function updateProduct($id, Request $request) //update product
    {
        $product = Product::find($id);
        $product->update($request->all());
        return view('products.show', compact('product'));
    }
    public function updateProductImage($id, Request $input) //update product
    {

        $product = Product::find($id);
        $image = $input->file('image');
        $upload = 'uploads/products';
        $filename = $image->getClientOriginalName();
        $success = $image->move($upload, $filename);

        if($success){
            $product->image = $filename;
            $product->update();
            $product->save();
            //return redirect('products');
            return view('products.show', compact('product'));

        }
        else{
            dd("product niet aangemaakt");
        }
    }
    public function softDelete($id, Request $request)
    {

        $product = Product::find($id);

        $product->softDeletes($request->all());

        return redirect('products');
    }

    public function restore($id)
    {

        Product::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect('backoffice/products');
    }

    public function importExcel()
    {
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    Product::create([
                        'product_code' => $value->product_code,
                        'name' => $value->name,
                        'brand' => $value->brand,
                        'brand_number' => $value->brand_number,
                        'serial_number' => $value->serial_number,
                        'production_date' => $value->production_date,
                        'description' => $value->description,
                        'tags' => $value->tags,
                        'QR_code' => $value->QR_code,
                        'purchase_price' => $value->purchase_price,
                        'purchase_date' => $value->purchase_date,
                        'CanBeLentByTeacher' => $value->CanBeLentByTeacher,
                        'CanBeLentByStudent' => $value->CanBeLentByStudent,
                        'type' => $value->type,
                        'active' => $value->active,
                        'image' => $value->image,
                    ]);
                }
            }
            else{
                dd("fout in het bestand, kijk na of de titels juist staan, download het voorbeeld.");
            }
        }
        return redirect('backoffice/products');
    }


}
