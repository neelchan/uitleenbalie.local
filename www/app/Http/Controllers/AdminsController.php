<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Input;
use Kyslik\ColumnSortable;
use Redirect;
use Session;
use Validator;
use Excel;

class AdminsController extends Controller
{
    public function index(Admin $admin)
    {
        $sortby = Input::get('sortby');
        $order = Input::get('order');

        if ($sortby && $order) {
            $admins = $admin->orderBy($sortby, $order)->paginate(10);
        } else {
            $admins = $admin->paginate(10);
        }

        return view('users.admins.index', compact('admins', 'sortby', 'order'));
    }

    public function Inactive(Admin $admin)
    {

        $sortby = Input::get('sortby');
        $order = Input::get('order');

        if ($sortby && $order) {
            $admins = $admin->where('disabled', '1')->orderBy($sortby, $order)->paginate(10);
        } else {
            $admins = $admin->where('disabled', '1')->paginate(10);
        }

        return view('users.admins.inactive', compact('admins', 'sortby', 'order'));

    }

    public function showSoftDeleteResults(Admin $admin)
    {

        $sortby = Input::get('sortby');
        $order = Input::get('order');

        if ($sortby && $order) {
            $admins = $admin->onlyTrashed()->orderBy($sortby, $order)->paginate(10);
        } else {
            $admins = $admin->onlyTrashed()->paginate(10);
        }

        return view('users.admins.deleted', compact('admins', 'sortby', 'order'));
    }

    public function show($id)
    {
        $admin = Admin::findOrFail($id);

        return view('users.admins.show', compact('admin'));
    }

    public function newAdmin()
    {
        return view('users.admins.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $input)
    {
        $table = new Admin();
        $table->name = $input->Input('name');
        $table->email = $input->Input('email');
        $table->password = bcrypt($input->Input('password'));

        $table->save();

        return redirect('users.admins');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editAdmin($id)
    {
        $admin = Admin::find($id);
        return view('users.admins.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateAdmin(Request $request, $id)
    {
        $admin = Admin::find($id);
        $admin->update($request->all());
        return view('users.admins.show', compact('admin'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAdmin($id, Request $request)
    {
        $admin = Admin::find($id);
        $admin->delete($request->all());
        return redirect('users.admins');

    }

    public function softDelete($id, Request $request)
    {
        $admin = Admin::find($id);
        $admin->softDeletes($request->all());
        return redirect('users.admins');
    }

    public function restore($id)
    {

        Admin::withTrashed()
            ->where('id', $id)
            ->restore();

        return redirect('users.admins');
    }

    public function export(){
        Excel::create('Admins', function($excel) {

            $excel->sheet('Admins', function($sheet) {
                $admins = Admin::orderBy('created_at','desc')->get();
                $sheet->loadView('admins.csv', ['admins' => $admins->toArray()]);
            });
        })->download('xls');
        //!!!! verwijderde gebruikers worden niet geexporteerd
    }

    public function importExcel()
    {
        if(Input::hasFile('import_file')){
            $path = Input::file('import_file')->getRealPath();
            $data = Excel::load($path, function($reader) {
            })->get();
            if(!empty($data) && $data->count()){
                foreach ($data as $key => $value) {
                    Admin::create([
                        'name' => $value->name,
                        'first_name' => $value->first_name,
                        'last_name' => $value->last_name,
                        'student_number' => $value->student_number,
                        'group' => $value->group,
                        'email' => $value->email,
                        'active' => $value->active,
                        'disabled' => $value->disabled,
                    ]);
                }
            }
        }
        return redirect('users.admins');
    }
}
