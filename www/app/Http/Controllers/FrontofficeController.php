<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Collection;
use App\Product;
use App\User;

//use App\Http\Requests;

use Kyslik\ColumnSortable;


use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;


class FrontofficeController extends Controller
{

    /**
     * Show the Front Office to the user.
     *
     * @return Response
     */
    public function index()
    {

        $latestProducts = $this->getLatestProducts();
        $mostRentedProducts = $this->getMostRentedProducts();
        $products_brand =  DB::table('products')->pluck('brand', 'name');
        $products_type =  DB::table('products')->pluck('type', 'name');


        return view('frontoffice')->with(['latestProducts' => $latestProducts, 'mostRentedProducts' => $mostRentedProducts, 'products_brand' => $products_brand, 'products_type' => $products_type]);

    }
    public function getLatestProducts(){


        $results = DB::table('products')->where('deleted_at', null)->orderBy('created_at', 'desc')->get();

        $collection = new Collection($results);

        $top5 = $collection->take(3);

        return $top5;
    }

    public function getMostRentedProducts(){


        $results = DB::table('products')->where('deleted_at', null)->orderBy('created_at', 'desc')->get();

        $collection = new Collection($results);

        $MostRented = $collection->take(6);

        return $MostRented;
    }

//    public function catalogus(Product $product)
//    {
//        $products = $product->sortable()->paginate(12);
//        //$products = $product->orderBy('created_at', 'desc')->paginate(12);
//
//        return view('frontoffice.catalogus', compact('products'));
//    }

    public function catalogus()
    {
       $products =  DB::table('products')->orderBy('created_at', 'desc')->whereNull('deleted_at')->paginate(12);
        //$products = $product->orderBy('created_at', 'desc')->paginate(12);

        return view('frontoffice.catalogus', compact('products'));


    }

    public function search(Request $request)
    {
        $products =  DB::table('products')->where(function($query) use ($request) {

            if ( ($term = $request->get('term')) ) {
                $query->orWhere('name', 'like', '%' . $term . '%');
                $query->orWhere('brand', 'like', '%' . $term . '%');
                $query->orWhere('product_code', 'like', '%' . $term . '%');
                $query->orWhere('type', 'like', '%' . $term . '%');
            }
        })
            ->orderBy('created_at', 'desc')
            ->whereNull('deleted_at')
            ->paginate(12);

        $products_brand =  DB::table('products')->pluck('brand', 'name');
        $products_type =  DB::table('products')->pluck('type', 'name');

        //return view('frontoffice.catalogus', compact('products'));
        return view('frontoffice.catalogus')->with(['products' => $products, 'products_brand' => $products_brand, 'products_type' => $products_type]);

    }


    public function show($id)
    {
        $product = DB::table('products')->find($id);

        return view('frontoffice.products.show', compact('product'));
    }

    public function account($id)
    {
        $user = User::find($id);

        return view('frontoffice.account', compact('user'));
    }
    public function editUser($id) //view edit user page
    {
        $user = User::find($id);
        return view('frontoffice.account.edit', compact('user'));
    }
    public function editUserImage($id) //view edit product page
    {
        $user = User::find($id);
        return view('frontoffice.account.editImage', compact('user'));
    }
    public function updateUser($id, Request $request) //update user
    {
        $user = User::find($id);
        $user->update($request->all());
        return view('frontoffice.account', compact('user'));
    }
    public function updateUserImage($id, Request $input) //update user image
    {

        $user = User::find($id);
        $image = $input->file('image');
        $upload = 'uploads/users';
        $filename = $image->getClientOriginalName();
        $success = $image->move($upload, $filename);

        if($success){
            $user->image = $filename;
            $user->update();
            $user->save();
            return view('frontoffice.account', compact('user'));

        }
        else{
            dd("user niet aangemaakt");
        }
    }

    public function contact()
    {
        return view('frontoffice.contact');
    }
}