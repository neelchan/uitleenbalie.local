<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;

use Input;
use Validator;
use Redirect;
use Session;

use Kyslik\ColumnSortable;


class CalendarController extends Controller
{
    /**
     * Show the Backoffice Office to the user.
     *
     * @return Response
     */
    public function calendar()
    {
        return view('backoffice.calendar');
    }
}