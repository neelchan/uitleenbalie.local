<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Carbon\Carbon;

class Supplier extends Model
{
    use Sortable;
    use SoftDeletes;

    protected $fillable = [
        'company_name', 'telephone_number', 'global_email', 'description'
    ];

    protected $sortable = ['id',
        'company_name', 'telephone_number', 'global_email', 'description',
    ];

    protected $dates = ['deleted_at', 'created_at', 'updated_at', 'production_date', 'purchase_date'];
}
