<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Kyslik\ColumnSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Admin extends Authenticatable
{
    use Sortable;
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'first_name', 'last_name', 'student_number', 'group', 'email', 'password', 'active', 'disabled'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $sortable = ['id',
        'name',
        'email',
        'created_at',
        'updated_at'];

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
}
