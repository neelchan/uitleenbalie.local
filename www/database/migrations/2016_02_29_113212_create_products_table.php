<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('product_code');
            $table->string('name');
            $table->string('brand');
            $table->string('brand_number')->nullable();
            $table->string('serial_number')->nullable();
            $table->timestamp('production_date')->nullable();
            $table->string('image');
            $table->text('description');
            $table->string('tags');
            $table->string('QR_code')->nullable();
            $table->string('purchase_price');
            $table->timestamp('purchase_date');
            $table->boolean('CanBeLentByTeacher')->nullable();
            $table->boolean('CanBeLentByStudent')->nullable();
            $table->enum('type', array('AVM', 'ICT'));
            $table->boolean('active')->default('1')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
