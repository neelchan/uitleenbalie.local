<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $limit = 10;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('users')->insert([ //,
                'name' => $faker->userName,
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'student_number' => $faker->numberBetween($min = 10000, $max = 90000),
                'group' => $faker->word,
                'email' => $faker->unique()->email,
                'password' => $faker->password,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
        DB::table('users')->insert([ //,
            'name' => 'neelchan',
            'first_name' => 'Neeltje',
            'last_name' => 'Chanterie',
            'student_number' => '78615',
            'group' => '3MMP-proDEV',
            'email' => 'neeltje.ch@gmail.com',
            'password' => bcrypt('secret'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
        DB::table('users')->insert([ //,
            'name' => 'admin',
            'first_name' => 'admin',
            'last_name' => 'admin',
            'student_number' => '00000',
            'group' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
        ]);
    }
}
