<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $limit = 5;

        $faker = Faker\Factory::create();

        for ($i = 0; $i < $limit; $i++) {
            DB::table('products')->insert([
                'product_code' => $faker->ean8,
                'name' => 'Toshiba Satellite',
                'brand' => 'Toshiba',
                'brand_number' => $faker->ean13,
                'serial_number' => $faker->ean13,
                'production_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'image' => '819-eP+0bwL._SX522_.jpg',
                'description' => '15.6-inch Laptop (2.1 GHz Intel Celeron N2830 Processor, 4GB DIMM, 500GB HDD, Windows 8.1) Jet Black',
                'tags' => 'laptop, toshiba',
                'QR_code' => 'C55-B5298',
                'purchase_price' => '359.85',
                'purchase_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'CanBeLentByTeacher' => '1',
                'CanBeLentByStudent' => '1',
                'type' => 'ICT',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
        for ($i = 0; $i < $limit; $i++) {
            DB::table('products')->insert([
                'product_code' => $faker->ean8,
                'name' => 'Canon EOS 5D Mark III',
                'brand' => 'Canon',
                'brand_number' => $faker->ean13,
                'serial_number' => $faker->ean13,
                'production_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'image' => '300px-Canon_EOS_5D_Mark_III.jpg',
                'description' => 'The Canon EOS 5D Mark III is a professional full-frame digital single-lens reflex (DSLR) camera made by Canon. It has a 22.3 megapixel CMOS image sensor.  Succeeding the EOS 5D Mark II, it was announced on 2 March 2012,[2] the 25th anniversary of the announcement of the first camera in the EOS line, the EOS 650. It was also Canon\'s 75th anniversary.[3] The Mark III went on sale later in March with a retail price of $3,499 in the US, £2999 in the UK, and €3569 in the Eurozone.',
                'tags' => 'foto, camera, filmen, canon',
                'QR_code' => '100–25600',
                'purchase_price' => '3569',
                'purchase_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'CanBeLentByTeacher' => '1',
                'CanBeLentByStudent' => '1',
                'type' => 'AVM',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),

            ]);
        }
        for ($i = 0; $i < $limit; $i++) {
            DB::table('products')->insert([
                'product_code' => $faker->ean8,
                'name' => 'HP Officejet H470 Mobile Printers',
                'brand' => 'HP',
                'brand_number' => $faker->ean13,
                'serial_number' => $faker->ean13,
                'production_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'image' => 'hp-portable-printer.jpg',
                'description' => 'HP has long been one of the top brands of computer printers, and it should be no surprise that they make one of the best portable printers for notebook computers. Their OfficeJet H470 Mobile Printer series all feature wireless, Bluetooth printing, and USB connectivity options, plus they come with 32 megabytes of memory. They can print up to 22 ppm (pages per minute) in black and white at 1200 x 1200 dpi, and up to 18 ppm in color at 4800 optimized dpi. With all those features, it only weighs five pounds and measures approximately 13 x 7 x 3.',
                'tags' => 'printer, hp, foto',
                'QR_code' => 'H470-002',
                'purchase_price' => '3569',
                'purchase_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'CanBeLentByTeacher' => '1',
                'CanBeLentByStudent' => '1',
                'type' => 'ICT',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),

            ]);
        }
        for ($i = 0; $i < $limit; $i++) {
            DB::table('products')->insert([
                'product_code' => $faker->ean8,
                'name' => 'Lowel DV Creator 55 Light Kit',
                'brand' => 'Lowel DV',
                'brand_number' => $faker->ean13,
                'serial_number' => $faker->ean13,
                'production_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'image' => 'image.jpeg',
                'description' => 'Lowel\'s DV Creator 55 Kit combines some of their most popular lights with accessories for increased versatility. A 500w Rifa eX 55 collapsible softlight, a 500w max. focusable Omni-light, a 250w focusable Pro-light & a 750w max. broad throw Tota-light, with Uni-stands and a handful of light controls in a compact hard case.',
                'tags' => 'filmen, kit, belichitng',
                'QR_code' => 'DV-9034Z',
                'purchase_price' => '1596.95',
                'purchase_date' => Carbon::now()->format('Y-m-d H:i:s'),
                'CanBeLentByTeacher' => '1',
                'CanBeLentByStudent' => '1',
                'type' => 'AVM',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),

            ]);
        }
    }

}
