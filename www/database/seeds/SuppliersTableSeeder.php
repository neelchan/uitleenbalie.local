<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SuppliersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Faker\Factory::create();

        $limit = 33;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('suppliers')->insert([ //,
                'company_name' => $faker->name,
                'telephone_number' => $faker->phoneNumber,
                'global_email' => $faker->unique()->email,
                'description' => 'company',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }

    }
}
